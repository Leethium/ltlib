package openapi

type SecurityScheme struct {
	BearerFormat     string      `json:"bearerFormat,omitempty" yaml:"bearerFormat,omitempty"`
	Description      string      `json:"description,omitempty" yaml:"description,omitempty"`
	In               string      `json:"in,omitempty" yaml:"in,omitempty"`
	Name             string      `json:"name,omitempty" yaml:"name,omitempty"`
	Scheme           string      `json:"scheme,omitempty" yaml:"scheme,omitempty"`
	Type             string      `json:"type" yaml:"type"`
	Flows            *OAuthFlows `json:"flows,omitempty" yaml:"flows,omitempty"`
	OpenIdConnectUrl string      `json:"openIdConnectUrl,omitempty" yaml:"openIdConnectUrl,omitempty"`
}

// OAuthFlows
type OAuthFlows struct {
	AuthorizationCode *AuthorizationCodeOAuthFlow `json:"authorizationCode,omitempty" yaml:"authorizationCode,omitempty"`
	ClientCredentials *ClientCredentialsFlow      `json:"clientCredentials,omitempty" yaml:"clientCredentials,omitempty"`
	Implicit          *ImplicitOAuthFlow          `json:"implicit,omitempty" yaml:"implicit,omitempty"`
	Password          *PasswordOAuthFlow          `json:"password,omitempty" yaml:"password,omitempty"`
}

// ClientCredentialsFlow
type ClientCredentialsFlow struct {
	RefreshUrl string            `json:"refreshUrl,omitempty" yaml:"refreshUrl,omitempty"`
	Scopes     map[string]string `json:"scopes,omitempty" yaml:"scopes,omitempty"`
	TokenUrl   string            `json:"tokenUrl" yaml:"tokenUrl"`
}

// ImplicitOAuthFlow
type ImplicitOAuthFlow struct {
	AuthorizationUrl string            `json:"authorizationUrl" yaml:"authorizationUrl"`
	RefreshUrl       string            `json:"refreshUrl,omitempty" yaml:"refreshUrl,omitempty"`
	Scopes           map[string]string `json:"scopes" yaml:"scopes"`
}

// PasswordOAuthFlow
type PasswordOAuthFlow struct {
	RefreshUrl string            `json:"refreshUrl,omitempty" yaml:"refreshUrl,omitempty"`
	Scopes     map[string]string `json:"scopes,omitempty" yaml:"scopes,omitempty"`
	TokenUrl   string            `json:"tokenUrl" yaml:"tokenUrl"`
}

// AuthorizationCodeOAuthFlow
type AuthorizationCodeOAuthFlow struct {
	AuthorizationUrl string            `json:"authorizationUrl" yaml:"authorizationUrl"`
	RefreshUrl       string            `json:"refreshUrl,omitempty" yaml:"refreshUrl,omitempty"`
	Scopes           map[string]string `json:"scopes,omitempty" yaml:"scopes,omitempty"`
	TokenUrl         string            `json:"tokenUrl" yaml:"tokenUrl"`
}
