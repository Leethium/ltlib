package providerlib

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/leethium/ltlib/lib/openapi"
	"gitlab.com/leethium/ltlib/lib/routemaker"
)

type provPathParamStringStruct struct {
	ParamName string
}

func (pp *provPathParamStringStruct) Provide(resp http.ResponseWriter, req *http.Request, prevProvided []any) (output string, next bool) {
	if len(prevProvided) == 0 {
		return "", false // Send error here
	}
	params, ok := prevProvided[0].(httprouter.Params)
	if !ok {
		return "", false // Send error here
	}
	return params.ByName(pp.ParamName), true
}

func (pp *provPathParamStringStruct) ApplyDocumentation(*openapi.OpenApiDoc, *openapi.Operation) error {
	return nil
}

func ProvideParamString(key string) routemaker.Provider[string] {
	out := &provPathParamStringStruct{}

	_ = routemaker.Documentable(out) // Ensure provider is documentable
	return out
}
