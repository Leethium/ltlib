package ltlib

import (
	"errors"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt"
)

var ErrorJWTTokenInvalid = errors.New("JWT Token not valid")

var JWT *JWTGenerator

type JWTGenerator struct {
	method     jwt.SigningMethod
	signingKey []byte
}

func init() {
	Hooks.Add("ltlib.PostSQLMigrate", "initJWT", initJWT)
}

func initJWT(...any) {
	jwt := &JWTGenerator{
		method: jwt.SigningMethodHS256,
	}

	token := ""
	DB.Get(&token, "SELECT data FROM ltlibmeta WHERE id=?", "ltlib.JWTSigningKey")
	if len(token) == 0 {
		Warn("msg", "ltlib.JWTSigningKey not found, generating")
	} else {
		Info("msg", "ltlib.JWTSigningKey loaded")
		jwt.signingKey = []byte(token)
		JWT = jwt
		return
	}

	DB.Exec("INSERT INTO ltlibmeta(id, data) VALUES (?, ?)", "ltlib.JWTSigningKey", GenerateRandomString(32))
	// We don't care about any error that could happen here, if 2 instances try to generate a key,
	// only the first will be saved and the value will be loaded on the next run of initJWT()
	initJWT()
}

func (jg *JWTGenerator) Sign(claims map[string]any, exp time.Duration) string {
	token := jwt.New(jg.method)
	tkclaims := token.Claims.(jwt.MapClaims)
	for k, v := range claims {
		tkclaims[k] = v
	}
	tkclaims["exp"] = time.Now().Add(exp).Unix()

	tokenString, err := token.SignedString(jg.signingKey)
	if err != nil {
		panic(err)
	}
	return tokenString
}

func (jg *JWTGenerator) Parse(token string) (map[string]any, error) {
	jtoken, err := jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error in parsing")
		}
		return jg.signingKey, nil
	})

	if err != nil {
		return nil, ErrorJWTTokenInvalid
	}
	if !jtoken.Valid {
		return jtoken.Claims.(jwt.MapClaims), ErrorJWTTokenInvalid
	}

	return jtoken.Claims.(jwt.MapClaims), nil
}
