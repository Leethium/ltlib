package ltlib

var Mailer mailer

type mailer interface {
	Send(to string, subject string, html, text string) error
}

var (
	mailSender  = ""
	mailCharset = "UTF-8"
	mailBCC     = []string{}
)

func init() {
	KP.Flag("mail.bcc", "Mail BCC for debug purposes").Envar("MAIL_BCC").StringsVar(&mailBCC)
	KP.Flag("mail.from", "Mail sender email address").Envar("MAIL_FROM").StringVar(&mailSender)
}
