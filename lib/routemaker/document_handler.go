package routemaker

import (
	"encoding/json"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/leethium/ltlib/lib/ltlib"
	"gitlab.com/leethium/ltlib/lib/openapi"
	"gopkg.in/yaml.v3"
)

var docMemoization = ""

func GetDocumentationOutputYML() string {
	if docMemoization == "" {
		docLock.Lock()
		defer docLock.Unlock()
		asJson := map[string]any{} // Hacky workaround: most of the structs used in providers won't have YAML annotations
		{
			btTmp, _ := json.Marshal(documentation)
			json.Unmarshal(btTmp, &asJson)
		}
		btbuf := strings.Builder{}
		yenc := yaml.NewEncoder(&btbuf)
		yenc.SetIndent(2)
		err := yenc.Encode(asJson)
		if err != nil {
			ltlib.Error("msg", "doc output", "err", err)
		}
		docMemoization = btbuf.String()
	}

	return docMemoization
}

func DocumentationFileHandler(inputFile string) {
	if inputFile == "" {
		// Init new documentation
		documentation = &openapi.OpenApiDoc{}
	} else {
		LoadDocumentation(inputFile)
	}
	handleGlobalDocInfos(documentation)
}

func DocumentationYMLHandler(input string) {
	if input == "" {
		// Init new documentation
		documentation = &openapi.OpenApiDoc{}
	} else {
		LoadDocumentationFromYAMLString(input)
	}
	handleGlobalDocInfos(documentation)
}

func DocumentationJSONHandler(input string) {
	if input == "" {
		// Init new documentation
		documentation = &openapi.OpenApiDoc{}
	} else {
		LoadDocumentationFromJSONString(input)
	}
	handleGlobalDocInfos(documentation)
}

func LoadDocumentation(file string) {
	docLock.Lock()
	defer docLock.Unlock()
	if documentation != nil {
		ltlib.Fatal("msg", "documentation loaded twice")
	}
	ftype := ""
	switch filepath.Ext(file) {
	case ".yaml", ".yml", ".YML", ".YAML":
		ftype = "yaml"
	case ".json", ".js", ".JSON":
		ftype = "json"
	default:
		ltlib.Fatal("msg", "could not identify file type for ext", "ext", filepath.Ext(file))
	}

	schema, err := os.Open(file)
	ltlib.FatalIf(err)

	docTmp := openapi.OpenApiDoc{}
	if ftype == "json" {
		jsd := json.NewDecoder(schema)
		jsd.DisallowUnknownFields()
		err := jsd.Decode(&docTmp)
		ltlib.FatalIf(err)
	} else if ftype == "yml" {
		ysd := yaml.NewDecoder(schema)
		ysd.KnownFields(true)
		err := ysd.Decode(&docTmp)
		ltlib.FatalIf(err)
	}
	documentation = &docTmp
	runStack()
}

func LoadDocumentationFromJSONString(jsonin string) {
	docLock.Lock()
	defer docLock.Unlock()
	if documentation != nil {
		ltlib.Fatal("msg", "documentation loaded twice")
	}
	docTmp := openapi.OpenApiDoc{}
	jsd := json.NewDecoder(strings.NewReader(jsonin))
	jsd.DisallowUnknownFields()
	err := jsd.Decode(&docTmp)
	ltlib.FatalIf(err)
	documentation = &docTmp
	runStack()
}

func LoadDocumentationFromYAMLString(yamlin string) {
	docLock.Lock()
	defer docLock.Unlock()
	if documentation != nil {
		ltlib.Fatal("msg", "documentation loaded twice")
	}
	docTmp := openapi.OpenApiDoc{}
	ysd := yaml.NewDecoder(strings.NewReader(yamlin))
	ysd.KnownFields(true)
	err := ysd.Decode(&docTmp)
	ltlib.FatalIf(err)
	documentation = &docTmp
	runStack()
}

func populateDoc() {
	if documentation.Components == nil {
		documentation.Components = &openapi.Components{
			Callbacks:       make(map[openapi.ComponentName]*openapi.Callback),
			Examples:        make(map[openapi.ComponentName]*openapi.Example),
			Headers:         make(map[openapi.ComponentName]*openapi.Header),
			Links:           make(map[openapi.ComponentName]*openapi.Link),
			Parameters:      make(map[openapi.ComponentName]*openapi.Parameter),
			RequestBodies:   make(map[openapi.ComponentName]*openapi.RequestBody),
			Responses:       make(map[openapi.ComponentName]*openapi.Response),
			Schemas:         make(map[openapi.ComponentName]*openapi.Schema),
			SecuritySchemes: make(map[openapi.ComponentName]*openapi.SecurityScheme),
		}
	}
	if documentation.Components.Callbacks == nil {
		documentation.Components.Callbacks = make(map[openapi.ComponentName]*openapi.Callback)
	}
	if documentation.Components.Examples == nil {
		documentation.Components.Examples = make(map[openapi.ComponentName]*openapi.Example)
	}
	if documentation.Components.Headers == nil {
		documentation.Components.Headers = make(map[openapi.ComponentName]*openapi.Header)
	}
	if documentation.Components.Links == nil {
		documentation.Components.Links = make(map[openapi.ComponentName]*openapi.Link)
	}
	if documentation.Components.Parameters == nil {
		documentation.Components.Parameters = make(map[openapi.ComponentName]*openapi.Parameter)
	}
	if documentation.Components.RequestBodies == nil {
		documentation.Components.RequestBodies = make(map[openapi.ComponentName]*openapi.RequestBody)
	}
	if documentation.Components.Responses == nil {
		documentation.Components.Responses = make(map[openapi.ComponentName]*openapi.Response)
	}
	if documentation.Components.Schemas == nil {
		documentation.Components.Schemas = make(map[openapi.ComponentName]*openapi.Schema)
	}
	if documentation.Components.SecuritySchemes == nil {
		documentation.Components.SecuritySchemes = make(map[openapi.ComponentName]*openapi.SecurityScheme)
	}
	if documentation.Paths == nil {
		documentation.Paths = make(map[openapi.HttpPath]map[openapi.HttpMethod]*openapi.Operation)
	}
}

func runStack() {
	populateDoc()
	for _, v := range documentableOPStack {
		v(documentation)
	}
	for method, dst := range documentableStack {
		for path, dst2 := range dst {
			for _, docable := range dst2 {
				op := mustGetOperationByPath(method, path)
				err := docable.ApplyDocumentation(documentation, op)
				ltlib.FatalIf(err)
			}
		}
	}
}
