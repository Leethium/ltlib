package ltlib

var useStubMailer bool

type stubmailer struct{}

func init() {
	KP.Flag("mailer.withstub", "Use a stub implementation of a mailer for testing purposes").Envar("MAIL_STUB").BoolVar(&useStubMailer)
	Hooks.Add("ltlib.PostInit", "initMailerSTUB", func(a ...any) {
		if !useStubMailer {
			return
		}
		if Mailer != nil {
			Fatal("msg", "Attempted to register multiple mailers")
		}
		Mailer = &stubmailer{}
		Info("msg", "Mailer initialised", "provider", "STUB")
	})
}

func (*stubmailer) Send(to string, subject string, html, text string) error {
	Info("msg", "mail requested", "subject", subject, "to", to)
	return nil
}
