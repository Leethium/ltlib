package routemaker

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func Handle0(router *httprouter.Router, method Method, path string, cb func(resp http.ResponseWriter, req *http.Request), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p})
			if !next {
				return
			}
		}
		cb(w, r)
	})
}

func Handle1[T1 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], cb func(http.ResponseWriter, *http.Request, T1), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p})
			if !next {
				return
			}
		}

		cb(w, r, r1)
	})
}

func Handle2[T1, T2 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], cb func(http.ResponseWriter, *http.Request, T1, T2), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2)
	})
}

func Handle3[T1, T2, T3 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], cb func(http.ResponseWriter, *http.Request, T1, T2, T3), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3)
	})
}

func Handle4[T1, T2, T3, T4 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4)
	})
}

func Handle5[T1, T2, T3, T4, T5 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5)
	})
}

func Handle6[T1, T2, T3, T4, T5, T6 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6)
	})
}

func Handle7[T1, T2, T3, T4, T5, T6, T7 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7)
	})
}

func Handle8[T1, T2, T3, T4, T5, T6, T7, T8 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], p8 Provider[T8], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7, T8), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)
	handleDocumentableProvider(method, path, p8)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		r8, next := p8.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7, r8)
	})
}

func Handle9[T1, T2, T3, T4, T5, T6, T7, T8, T9 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], p8 Provider[T8], p9 Provider[T9], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7, T8, T9), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)
	handleDocumentableProvider(method, path, p8)
	handleDocumentableProvider(method, path, p9)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		r8, next := p8.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
		if !next {
			return
		}
		r9, next := p9.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7, r8, r9)
	})
}

func Handle10[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], p8 Provider[T8], p9 Provider[T9], p10 Provider[T10], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)
	handleDocumentableProvider(method, path, p8)
	handleDocumentableProvider(method, path, p9)
	handleDocumentableProvider(method, path, p10)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		r8, next := p8.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
		if !next {
			return
		}
		r9, next := p9.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8})
		if !next {
			return
		}
		r10, next := p10.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10)
	})
}

func Handle11[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], p8 Provider[T8], p9 Provider[T9], p10 Provider[T10], p11 Provider[T11], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)
	handleDocumentableProvider(method, path, p8)
	handleDocumentableProvider(method, path, p9)
	handleDocumentableProvider(method, path, p10)
	handleDocumentableProvider(method, path, p11)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		r8, next := p8.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
		if !next {
			return
		}
		r9, next := p9.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8})
		if !next {
			return
		}
		r10, next := p10.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9})
		if !next {
			return
		}
		r11, next := p11.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
	})
}

func Handle12[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], p8 Provider[T8], p9 Provider[T9], p10 Provider[T10], p11 Provider[T11], p12 Provider[T12], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)
	handleDocumentableProvider(method, path, p8)
	handleDocumentableProvider(method, path, p9)
	handleDocumentableProvider(method, path, p10)
	handleDocumentableProvider(method, path, p11)
	handleDocumentableProvider(method, path, p12)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		r8, next := p8.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
		if !next {
			return
		}
		r9, next := p9.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8})
		if !next {
			return
		}
		r10, next := p10.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9})
		if !next {
			return
		}
		r11, next := p11.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10})
		if !next {
			return
		}
		r12, next := p12.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
	})
}

func Handle13[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], p8 Provider[T8], p9 Provider[T9], p10 Provider[T10], p11 Provider[T11], p12 Provider[T12], p13 Provider[T13], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)
	handleDocumentableProvider(method, path, p8)
	handleDocumentableProvider(method, path, p9)
	handleDocumentableProvider(method, path, p10)
	handleDocumentableProvider(method, path, p11)
	handleDocumentableProvider(method, path, p12)
	handleDocumentableProvider(method, path, p13)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		r8, next := p8.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
		if !next {
			return
		}
		r9, next := p9.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8})
		if !next {
			return
		}
		r10, next := p10.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9})
		if !next {
			return
		}
		r11, next := p11.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10})
		if !next {
			return
		}
		r12, next := p12.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11})
		if !next {
			return
		}
		r13, next := p13.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
	})
}

func Handle14[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], p8 Provider[T8], p9 Provider[T9], p10 Provider[T10], p11 Provider[T11], p12 Provider[T12], p13 Provider[T13], p14 Provider[T14], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)
	handleDocumentableProvider(method, path, p8)
	handleDocumentableProvider(method, path, p9)
	handleDocumentableProvider(method, path, p10)
	handleDocumentableProvider(method, path, p11)
	handleDocumentableProvider(method, path, p12)
	handleDocumentableProvider(method, path, p13)
	handleDocumentableProvider(method, path, p14)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		r8, next := p8.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
		if !next {
			return
		}
		r9, next := p9.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8})
		if !next {
			return
		}
		r10, next := p10.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9})
		if !next {
			return
		}
		r11, next := p11.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10})
		if !next {
			return
		}
		r12, next := p12.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11})
		if !next {
			return
		}
		r13, next := p13.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12})
		if !next {
			return
		}
		r14, next := p14.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
	})
}

func Handle15[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], p8 Provider[T8], p9 Provider[T9], p10 Provider[T10], p11 Provider[T11], p12 Provider[T12], p13 Provider[T13], p14 Provider[T14], p15 Provider[T15], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)
	handleDocumentableProvider(method, path, p8)
	handleDocumentableProvider(method, path, p9)
	handleDocumentableProvider(method, path, p10)
	handleDocumentableProvider(method, path, p11)
	handleDocumentableProvider(method, path, p12)
	handleDocumentableProvider(method, path, p13)
	handleDocumentableProvider(method, path, p14)
	handleDocumentableProvider(method, path, p15)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		r8, next := p8.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
		if !next {
			return
		}
		r9, next := p9.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8})
		if !next {
			return
		}
		r10, next := p10.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9})
		if !next {
			return
		}
		r11, next := p11.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10})
		if !next {
			return
		}
		r12, next := p12.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11})
		if !next {
			return
		}
		r13, next := p13.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12})
		if !next {
			return
		}
		r14, next := p14.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13})
		if !next {
			return
		}
		r15, next := p15.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
	})
}

func Handle16[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], p8 Provider[T8], p9 Provider[T9], p10 Provider[T10], p11 Provider[T11], p12 Provider[T12], p13 Provider[T13], p14 Provider[T14], p15 Provider[T15], p16 Provider[T16], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)
	handleDocumentableProvider(method, path, p8)
	handleDocumentableProvider(method, path, p9)
	handleDocumentableProvider(method, path, p10)
	handleDocumentableProvider(method, path, p11)
	handleDocumentableProvider(method, path, p12)
	handleDocumentableProvider(method, path, p13)
	handleDocumentableProvider(method, path, p14)
	handleDocumentableProvider(method, path, p15)
	handleDocumentableProvider(method, path, p16)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		r8, next := p8.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
		if !next {
			return
		}
		r9, next := p9.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8})
		if !next {
			return
		}
		r10, next := p10.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9})
		if !next {
			return
		}
		r11, next := p11.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10})
		if !next {
			return
		}
		r12, next := p12.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11})
		if !next {
			return
		}
		r13, next := p13.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12})
		if !next {
			return
		}
		r14, next := p14.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13})
		if !next {
			return
		}
		r15, next := p15.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14})
		if !next {
			return
		}
		r16, next := p16.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16)
	})
}

func Handle17[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], p8 Provider[T8], p9 Provider[T9], p10 Provider[T10], p11 Provider[T11], p12 Provider[T12], p13 Provider[T13], p14 Provider[T14], p15 Provider[T15], p16 Provider[T16], p17 Provider[T17], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)
	handleDocumentableProvider(method, path, p8)
	handleDocumentableProvider(method, path, p9)
	handleDocumentableProvider(method, path, p10)
	handleDocumentableProvider(method, path, p11)
	handleDocumentableProvider(method, path, p12)
	handleDocumentableProvider(method, path, p13)
	handleDocumentableProvider(method, path, p14)
	handleDocumentableProvider(method, path, p15)
	handleDocumentableProvider(method, path, p16)
	handleDocumentableProvider(method, path, p17)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		r8, next := p8.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
		if !next {
			return
		}
		r9, next := p9.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8})
		if !next {
			return
		}
		r10, next := p10.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9})
		if !next {
			return
		}
		r11, next := p11.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10})
		if !next {
			return
		}
		r12, next := p12.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11})
		if !next {
			return
		}
		r13, next := p13.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12})
		if !next {
			return
		}
		r14, next := p14.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13})
		if !next {
			return
		}
		r15, next := p15.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14})
		if !next {
			return
		}
		r16, next := p16.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15})
		if !next {
			return
		}
		r17, next := p17.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
	})
}

func Handle18[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], p8 Provider[T8], p9 Provider[T9], p10 Provider[T10], p11 Provider[T11], p12 Provider[T12], p13 Provider[T13], p14 Provider[T14], p15 Provider[T15], p16 Provider[T16], p17 Provider[T17], p18 Provider[T18], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)
	handleDocumentableProvider(method, path, p8)
	handleDocumentableProvider(method, path, p9)
	handleDocumentableProvider(method, path, p10)
	handleDocumentableProvider(method, path, p11)
	handleDocumentableProvider(method, path, p12)
	handleDocumentableProvider(method, path, p13)
	handleDocumentableProvider(method, path, p14)
	handleDocumentableProvider(method, path, p15)
	handleDocumentableProvider(method, path, p16)
	handleDocumentableProvider(method, path, p17)
	handleDocumentableProvider(method, path, p18)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		r8, next := p8.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
		if !next {
			return
		}
		r9, next := p9.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8})
		if !next {
			return
		}
		r10, next := p10.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9})
		if !next {
			return
		}
		r11, next := p11.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10})
		if !next {
			return
		}
		r12, next := p12.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11})
		if !next {
			return
		}
		r13, next := p13.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12})
		if !next {
			return
		}
		r14, next := p14.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13})
		if !next {
			return
		}
		r15, next := p15.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14})
		if !next {
			return
		}
		r16, next := p16.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15})
		if !next {
			return
		}
		r17, next := p17.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16})
		if !next {
			return
		}
		r18, next := p18.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)
	})
}

func Handle19[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], p8 Provider[T8], p9 Provider[T9], p10 Provider[T10], p11 Provider[T11], p12 Provider[T12], p13 Provider[T13], p14 Provider[T14], p15 Provider[T15], p16 Provider[T16], p17 Provider[T17], p18 Provider[T18], p19 Provider[T19], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)
	handleDocumentableProvider(method, path, p8)
	handleDocumentableProvider(method, path, p9)
	handleDocumentableProvider(method, path, p10)
	handleDocumentableProvider(method, path, p11)
	handleDocumentableProvider(method, path, p12)
	handleDocumentableProvider(method, path, p13)
	handleDocumentableProvider(method, path, p14)
	handleDocumentableProvider(method, path, p15)
	handleDocumentableProvider(method, path, p16)
	handleDocumentableProvider(method, path, p17)
	handleDocumentableProvider(method, path, p18)
	handleDocumentableProvider(method, path, p19)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		r8, next := p8.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
		if !next {
			return
		}
		r9, next := p9.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8})
		if !next {
			return
		}
		r10, next := p10.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9})
		if !next {
			return
		}
		r11, next := p11.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10})
		if !next {
			return
		}
		r12, next := p12.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11})
		if !next {
			return
		}
		r13, next := p13.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12})
		if !next {
			return
		}
		r14, next := p14.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13})
		if !next {
			return
		}
		r15, next := p15.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14})
		if !next {
			return
		}
		r16, next := p16.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15})
		if !next {
			return
		}
		r17, next := p17.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16})
		if !next {
			return
		}
		r18, next := p18.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17})
		if !next {
			return
		}
		r19, next := p19.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19)
	})
}

func Handle20[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20 any](router *httprouter.Router, method Method, path string, p1 Provider[T1], p2 Provider[T2], p3 Provider[T3], p4 Provider[T4], p5 Provider[T5], p6 Provider[T6], p7 Provider[T7], p8 Provider[T8], p9 Provider[T9], p10 Provider[T10], p11 Provider[T11], p12 Provider[T12], p13 Provider[T13], p14 Provider[T14], p15 Provider[T15], p16 Provider[T16], p17 Provider[T17], p18 Provider[T18], p19 Provider[T19], p20 Provider[T20], cb func(http.ResponseWriter, *http.Request, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	handleDocumentableProvider(method, path, p1)
	handleDocumentableProvider(method, path, p2)
	handleDocumentableProvider(method, path, p3)
	handleDocumentableProvider(method, path, p4)
	handleDocumentableProvider(method, path, p5)
	handleDocumentableProvider(method, path, p6)
	handleDocumentableProvider(method, path, p7)
	handleDocumentableProvider(method, path, p8)
	handleDocumentableProvider(method, path, p9)
	handleDocumentableProvider(method, path, p10)
	handleDocumentableProvider(method, path, p11)
	handleDocumentableProvider(method, path, p12)
	handleDocumentableProvider(method, path, p13)
	handleDocumentableProvider(method, path, p14)
	handleDocumentableProvider(method, path, p15)
	handleDocumentableProvider(method, path, p16)
	handleDocumentableProvider(method, path, p17)
	handleDocumentableProvider(method, path, p18)
	handleDocumentableProvider(method, path, p19)
	handleDocumentableProvider(method, path, p20)

	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		r1, next := p1.Provide(w, r, []any{p})
		if !next {
			return
		}
		r2, next := p2.Provide(w, r, []any{p, r1})
		if !next {
			return
		}
		r3, next := p3.Provide(w, r, []any{p, r1, r2})
		if !next {
			return
		}
		r4, next := p4.Provide(w, r, []any{p, r1, r2, r3})
		if !next {
			return
		}
		r5, next := p5.Provide(w, r, []any{p, r1, r2, r3, r4})
		if !next {
			return
		}
		r6, next := p6.Provide(w, r, []any{p, r1, r2, r3, r4, r5})
		if !next {
			return
		}
		r7, next := p7.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6})
		if !next {
			return
		}
		r8, next := p8.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7})
		if !next {
			return
		}
		r9, next := p9.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8})
		if !next {
			return
		}
		r10, next := p10.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9})
		if !next {
			return
		}
		r11, next := p11.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10})
		if !next {
			return
		}
		r12, next := p12.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11})
		if !next {
			return
		}
		r13, next := p13.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12})
		if !next {
			return
		}
		r14, next := p14.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13})
		if !next {
			return
		}
		r15, next := p15.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14})
		if !next {
			return
		}
		r16, next := p16.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15})
		if !next {
			return
		}
		r17, next := p17.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16})
		if !next {
			return
		}
		r18, next := p18.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17})
		if !next {
			return
		}
		r19, next := p19.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18})
		if !next {
			return
		}
		r20, next := p20.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19})
		if !next {
			return
		}
		for _, prov := range pany {
			_, next := prov.Provide(w, r, []any{p, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19})
			if !next {
				return
			}
		}

		cb(w, r, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
	})
}
