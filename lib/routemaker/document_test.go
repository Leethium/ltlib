package routemaker

import (
	"path/filepath"
	"testing"
)

func TestExt(t *testing.T) {
	if filepath.Ext("aaa.json") != ".json" {
		t.Error("How does this work")
	}
}
func TestNormalizePath(t *testing.T) {
	shouldExpect := []struct {
		in       string
		out      string
		params   []string
		catchall string
	}{
		{"/aaa/bbb", "/aaa/bbb", []string{}, ""},
		{"/aaa/bbb/:ccc", "/aaa/bbb/{ccc}", []string{"ccc"}, ""},
		{"/aaa/bbb/*ddd", "/aaa/bbb/{ddd}", []string{"ddd"}, "ddd"},
		{"/aaa/:bbb/ccc", "/aaa/{bbb}/ccc", []string{"bbb"}, ""},
		{"/aaa/:bbb/ccc/:ddd", "/aaa/{bbb}/ccc/{ddd}", []string{"bbb", "ddd"}, ""},
		{"/aaa/:bbb/ccc/*ddd", "/aaa/{bbb}/ccc/{ddd}", []string{"bbb", "ddd"}, "ddd"},
	}

	for k, v := range shouldExpect {
		out, params, catchall := normalizePath(v.in)
		if out != v.out {
			t.Fatalf("shouldExpect %v for %v out is %v expected %v", k, v.in, out, v.out)
		}
		if catchall != v.catchall {
			t.Fatalf("shouldExpect %v for %v catchall is %v expected %v", k, v.in, out, v.out)
		}
		for l, w := range params {
			if v.params[l] != w {
				t.Fatalf("shouldExpect %v for %v params[%v] is %v expected %v", k, v.in, l, w, v.params[l])
			}
		}
	}
}
