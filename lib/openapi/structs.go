package openapi

import "strconv"

type OpenApiDoc struct {
	Openapi      string                                 `json:"openapi" yaml:"openapi"`
	Info         *Info                                  `json:"info" yaml:"info"`
	Servers      []*Server                              `json:"servers,omitempty" yaml:"servers,omitempty"`
	Tags         []*Tag                                 `json:"tags,omitempty" yaml:"tags,omitempty"`
	Security     []*SecurityRequirement                 `json:"security,omitempty" yaml:"security,omitempty"`
	ExternalDocs map[string]*ExternalDocumentation      `json:"externalDocs,omitempty" yaml:"externalDocs,omitempty"`
	Paths        map[HttpPath]map[HttpMethod]*Operation `json:"paths" yaml:"paths"`
	Components   *Components                            `json:"components,omitempty" yaml:"components,omitempty"`
}

type Components struct {
	Callbacks       map[ComponentName]*Callback       `json:"callbacks,omitempty" yaml:"callbacks,omitempty"`
	Examples        map[ComponentName]*Example        `json:"examples,omitempty" yaml:"examples,omitempty"`
	Headers         map[ComponentName]*Header         `json:"headers,omitempty" yaml:"headers,omitempty"`
	Links           map[ComponentName]*Link           `json:"links,omitempty" yaml:"links,omitempty"`
	Parameters      map[ComponentName]*Parameter      `json:"parameters,omitempty" yaml:"parameters,omitempty"`
	RequestBodies   map[ComponentName]*RequestBody    `json:"requestBodies,omitempty" yaml:"requestBodies,omitempty"`
	Responses       map[ComponentName]*Response       `json:"responses,omitempty" yaml:"responses,omitempty"`
	Schemas         map[ComponentName]*Schema         `json:"schemas,omitempty" yaml:"schemas,omitempty"`
	SecuritySchemes map[ComponentName]*SecurityScheme `json:"securitySchemes,omitempty" yaml:"securitySchemes,omitempty"`
}

type Operation struct {
	Callbacks    map[string]any             `json:"callbacks,omitempty" yaml:"callbacks,omitempty"`
	Deprecated   bool                       `json:"deprecated,omitempty" yaml:"deprecated,omitempty"`
	Description  string                     `json:"description,omitempty" yaml:"description,omitempty"`
	ExternalDocs *ExternalDocumentation     `json:"externalDocs,omitempty" yaml:"externalDocs,omitempty"`
	OperationId  string                     `json:"operationId,omitempty" yaml:"operationId,omitempty"`
	Parameters   []*Parameter               `json:"parameters,omitempty" yaml:"parameters,omitempty"`
	RequestBody  *RequestBody               `json:"requestBody,omitempty" yaml:"requestBody,omitempty"`
	Responses    map[ResponseCode]*Response `json:"responses" yaml:"responses"`
	Security     []*SecurityRequirement     `json:"security,omitempty" yaml:"security,omitempty"`
	Servers      []*Server                  `json:"servers,omitempty" yaml:"servers,omitempty"`
	Summary      string                     `json:"summary,omitempty" yaml:"summary,omitempty"`
	Tags         []string                   `json:"tags,omitempty" yaml:"tags,omitempty"`
}

type ResponseCode string

func ToResponseCode(code uint) ResponseCode {
	return ResponseCode(strconv.FormatUint(uint64(code), 10))
}

type Schema struct {
	REF                  string                 `json:"$ref,omitempty" yaml:"$ref,omitempty"`
	AdditionalProperties any                    `json:"additionalProperties,omitempty" yaml:"additionalProperties,omitempty"`
	AllOf                []*Schema              `json:"allOf,omitempty" yaml:"allOf,omitempty"`
	AnyOf                []*Schema              `json:"anyOf,omitempty" yaml:"anyOf,omitempty"`
	Default              any                    `json:"default,omitempty" yaml:"default,omitempty"`
	Deprecated           bool                   `json:"deprecated,omitempty" yaml:"deprecated,omitempty"`
	Description          string                 `json:"description,omitempty" yaml:"description,omitempty"`
	Discriminator        *Discriminator         `json:"discriminator,omitempty" yaml:"discriminator,omitempty"`
	Enum                 []any                  `json:"enum,omitempty" yaml:"enum,omitempty"`
	Example              any                    `json:"example,omitempty" yaml:"example,omitempty"`
	ExclusiveMaximum     bool                   `json:"exclusiveMaximum,omitempty" yaml:"exclusiveMaximum,omitempty"`
	ExclusiveMinimum     bool                   `json:"exclusiveMinimum,omitempty" yaml:"exclusiveMinimum,omitempty"`
	ExternalDocs         *ExternalDocumentation `json:"externalDocs,omitempty" yaml:"externalDocs,omitempty"`
	Format               string                 `json:"format,omitempty" yaml:"format,omitempty"`
	Items                *Schema                `json:"items,omitempty" yaml:"items,omitempty"`
	MaxItems             int                    `json:"maxItems,omitempty" yaml:"maxItems,omitempty"`
	MaxLength            int                    `json:"maxLength,omitempty" yaml:"maxLength,omitempty"`
	MaxProperties        int                    `json:"maxProperties,omitempty" yaml:"maxProperties,omitempty"`
	Maximum              float64                `json:"maximum,omitempty" yaml:"maximum,omitempty"`
	MinItems             int                    `json:"minItems,omitempty" yaml:"minItems,omitempty"`
	MinLength            int                    `json:"minLength,omitempty" yaml:"minLength,omitempty"`
	MinProperties        int                    `json:"minProperties,omitempty" yaml:"minProperties,omitempty"`
	Minimum              float64                `json:"minimum,omitempty" yaml:"minimum,omitempty"`
	MultipleOf           float64                `json:"multipleOf,omitempty" yaml:"multipleOf,omitempty"`
	Not                  any                    `json:"not,omitempty" yaml:"not,omitempty"`
	Nullable             bool                   `json:"nullable,omitempty" yaml:"nullable,omitempty"`
	OneOf                []*Schema              `json:"oneOf,omitempty" yaml:"oneOf,omitempty"`
	Pattern              string                 `json:"pattern,omitempty" yaml:"pattern,omitempty"`
	Properties           map[string]*Schema     `json:"properties,omitempty" yaml:"properties,omitempty"`
	ReadOnly             bool                   `json:"readOnly,omitempty" yaml:"readOnly,omitempty"`
	Required             []string               `json:"required,omitempty" yaml:"required,omitempty"`
	Title                string                 `json:"title,omitempty" yaml:"title,omitempty"`
	Type                 string                 `json:"type,omitempty" yaml:"type,omitempty"`
	UniqueItems          bool                   `json:"uniqueItems,omitempty" yaml:"uniqueItems,omitempty"`
	WriteOnly            bool                   `json:"writeOnly,omitempty" yaml:"writeOnly,omitempty"`
	Xml                  *XML                   `json:"xml,omitempty" yaml:"xml,omitempty"`
}

func (sc *Schema) IsEmpty() bool {
	if sc.REF != "" {
		return false
	}
	if sc.AdditionalProperties != nil {
		return false
	}
	if sc.AllOf != nil {
		return false
	}
	if sc.AnyOf != nil {
		return false
	}
	if sc.Default != nil {
		return false
	}
	if sc.Deprecated != false {
		return false
	}
	if sc.Description != "" {
		return false
	}
	if sc.Discriminator != nil {
		return false
	}
	if sc.Enum != nil {
		return false
	}
	if sc.Example != nil {
		return false
	}
	if sc.ExclusiveMaximum != false {
		return false
	}
	if sc.ExclusiveMinimum != false {
		return false
	}
	if sc.ExternalDocs != nil {
		return false
	}
	if sc.Format != "" {
		return false
	}
	if sc.Items != nil {
		return false
	}
	if sc.MaxItems != 0 {
		return false
	}
	if sc.MaxLength != 0 {
		return false
	}
	if sc.MaxProperties != 0 {
		return false
	}
	if sc.Maximum != 0 {
		return false
	}
	if sc.MinItems != 0 {
		return false
	}
	if sc.MinLength != 0 {
		return false
	}
	if sc.MinProperties != 0 {
		return false
	}
	if sc.Minimum != 0 {
		return false
	}
	if sc.MultipleOf != 0 {
		return false
	}
	if sc.Not != nil {
		return false
	}
	if sc.Nullable != false {
		return false
	}
	if sc.OneOf != nil {
		return false
	}
	if sc.Pattern != "" {
		return false
	}
	if sc.Properties != nil {
		return false
	}
	if sc.ReadOnly != false {
		return false
	}
	if sc.Required != nil {
		return false
	}
	if sc.Title != "" {
		return false
	}
	if sc.Type != "" {
		return false
	}
	if sc.UniqueItems != false {
		return false
	}
	if sc.WriteOnly != false {
		return false
	}
	if sc.Xml != nil {
		return false
	}
	return true
}

type XML struct {
	Attribute bool   `json:"attribute,omitempty" yaml:"attribute,omitempty"`
	Name      string `json:"name,omitempty" yaml:"name,omitempty"`
	Namespace string `json:"namespace,omitempty" yaml:"namespace,omitempty"`
	Prefix    string `json:"prefix,omitempty" yaml:"prefix,omitempty"`
	Wrapped   bool   `json:"wrapped,omitempty" yaml:"wrapped,omitempty"`
}

type Discriminator struct {
	Mapping      map[string]string `json:"mapping,omitempty" yaml:"mapping,omitempty"`
	PropertyName string            `json:"propertyName" yaml:"propertyName"`
}

type RequestBody struct {
	Content     map[string]*MediaType `json:"content" yaml:"content"`
	Description string                `json:"description,omitempty" yaml:"description,omitempty"`
	Required    bool                  `json:"required,omitempty" yaml:"required,omitempty"`
}

type Response struct {
	Content     map[string]*MediaType `json:"content,omitempty" yaml:"content,omitempty"`
	Description string                `json:"description" yaml:"description"`
	Headers     map[string]any        `json:"headers,omitempty" yaml:"headers,omitempty"`
	Links       map[string]any        `json:"links,omitempty" yaml:"links,omitempty"`
}

type Parameter struct {
	AllowEmptyValue bool                  `json:"allowEmptyValue,omitempty" yaml:"allowEmptyValue,omitempty"`
	AllowReserved   bool                  `json:"allowReserved,omitempty" yaml:"allowReserved,omitempty"`
	Content         map[string]*MediaType `json:"content,omitempty" yaml:"content,omitempty"`
	Deprecated      bool                  `json:"deprecated,omitempty" yaml:"deprecated,omitempty"`
	Description     string                `json:"description,omitempty" yaml:"description,omitempty"`
	Example         any                   `json:"example,omitempty" yaml:"example,omitempty"`
	Examples        map[string]any        `json:"examples,omitempty" yaml:"examples,omitempty"`
	Explode         bool                  `json:"explode,omitempty" yaml:"explode,omitempty"`
	In              string                `json:"in" yaml:"in"`
	Name            string                `json:"name" yaml:"name"`
	Required        bool                  `json:"required,omitempty" yaml:"required,omitempty"`
	Schema          *Schema               `json:"schema,omitempty" yaml:"schema,omitempty"`
	Style           string                `json:"style,omitempty" yaml:"style,omitempty"`
}

type Link struct {
	Description  string         `json:"description,omitempty" yaml:"description,omitempty"`
	OperationId  string         `json:"operationId,omitempty" yaml:"operationId,omitempty"`
	OperationRef string         `json:"operationRef,omitempty" yaml:"operationRef,omitempty"`
	Parameters   map[string]any `json:"parameters,omitempty" yaml:"parameters,omitempty"`
	RequestBody  any            `json:"requestBody,omitempty" yaml:"requestBody,omitempty"`
	Server       *Server        `json:"server,omitempty" yaml:"server,omitempty"`
}

type ComponentName string

type Callback struct {
	AdditionalProperties map[string]*PathItem `json:"." yaml:"."`
}

type Header struct {
	AllowEmptyValue bool                  `json:"allowEmptyValue,omitempty" yaml:"allowEmptyValue,omitempty"`
	AllowReserved   bool                  `json:"allowReserved,omitempty" yaml:"allowReserved,omitempty"`
	Content         map[string]*MediaType `json:"content,omitempty" yaml:"content,omitempty"`
	Deprecated      bool                  `json:"deprecated,omitempty" yaml:"deprecated,omitempty"`
	Description     string                `json:"description,omitempty" yaml:"description,omitempty"`
	Example         any                   `json:"example,omitempty" yaml:"example,omitempty"`
	Examples        map[string]any        `json:"examples,omitempty" yaml:"examples,omitempty"`
	Explode         bool                  `json:"explode,omitempty" yaml:"explode,omitempty"`
	Required        bool                  `json:"required,omitempty" yaml:"required,omitempty"`
	Schema          any                   `json:"schema,omitempty" yaml:"schema,omitempty"`
	Style           string                `json:"style,omitempty" yaml:"style,omitempty"`
}

type Encoding struct {
	AllowReserved bool               `json:"allowReserved,omitempty" yaml:"allowReserved,omitempty"`
	ContentType   string             `json:"contentType,omitempty" yaml:"contentType,omitempty"`
	Explode       bool               `json:"explode,omitempty" yaml:"explode,omitempty"`
	Headers       map[string]*Header `json:"headers,omitempty" yaml:"headers,omitempty"`
	Style         string             `json:"style,omitempty" yaml:"style,omitempty"`
}

type MediaType struct {
	Encoding map[string]*Encoding `json:"encoding,omitempty" yaml:"encoding,omitempty"`
	Example  any                  `json:"example,omitempty" yaml:"example,omitempty"`
	Examples map[string]any       `json:"examples,omitempty" yaml:"examples,omitempty"`
	Schema   *Schema              `json:"schema,omitempty" yaml:"schema,omitempty"`
}

type Contact struct {
	Email string `json:"email,omitempty" yaml:"email,omitempty"`
	Name  string `json:"name,omitempty" yaml:"name,omitempty"`
	Url   string `json:"url,omitempty" yaml:"url,omitempty"`
}

type Example struct {
	Description   string `json:"description,omitempty" yaml:"description,omitempty"`
	ExternalValue string `json:"externalValue,omitempty" yaml:"externalValue,omitempty"`
	Summary       string `json:"summary,omitempty" yaml:"summary,omitempty"`
	Value         any    `json:"value,omitempty" yaml:"value,omitempty"`
}

type Info struct {
	Contact        *Contact `json:"contact,omitempty" yaml:"contact,omitempty"`
	Description    string   `json:"description,omitempty" yaml:"description,omitempty"`
	License        *License `json:"license,omitempty" yaml:"license,omitempty"`
	TermsOfService string   `json:"termsOfService,omitempty" yaml:"termsOfService,omitempty"`
	Title          string   `json:"title" yaml:"title"`
	Version        string   `json:"version" yaml:"version"`
}

// License
type License struct {
	Name string `json:"name" yaml:"name"`
	Url  string `json:"url,omitempty" yaml:"url,omitempty"`
}

type Tag struct {
	Description  string                 `json:"description,omitempty" yaml:"description,omitempty"`
	ExternalDocs *ExternalDocumentation `json:"externalDocs,omitempty" yaml:"externalDocs,omitempty"`
	Name         string                 `json:"name" yaml:"name"`
}

type ExternalDocumentation struct {
	Description string `json:"description,omitempty" yaml:"description,omitempty"`
	Url         string `json:"url" yaml:"url"`
}

type SecurityRequirement map[string][]string

type HttpPath string

type HttpMethod string

type PathItem struct {
	Description string    `json:"description,omitempty" yaml:"description,omitempty"`
	Parameters  []any     `json:"parameters,omitempty" yaml:"parameters,omitempty"`
	Ref         string    `json:"$ref,omitempty" yaml:"$ref,omitempty"`
	Servers     []*Server `json:"servers,omitempty" yaml:"servers,omitempty"`
	Summary     string    `json:"summary,omitempty" yaml:"summary,omitempty"`
}

type Server struct {
	Description string                     `json:"description,omitempty" yaml:"description,omitempty"`
	Url         string                     `json:"url" yaml:"url"`
	Variables   map[string]*ServerVariable `json:"variables,omitempty" yaml:"variables,omitempty"`
}

type ServerVariable struct {
	Default     string   `json:"default" yaml:"default"`
	Description string   `json:"description,omitempty" yaml:"description,omitempty"`
	Enum        []string `json:"enum,omitempty" yaml:"enum,omitempty"`
}
