package ltlib

import (
	"os"
	"path/filepath"

	"gopkg.in/alecthomas/kingpin.v2"
)

// KP is the exported kingpin app, use it before calling init
var KP = kingpin.New(filepath.Base(os.Args[0]), "API")

var config = struct {
	env string
}{}

var BaseURL = ""

// Config is the exported config struct
var Config struct {
	Prod       bool
	ReportMail string
}

func init() {
	KP.Version(Ver())
	KP.HelpFlag.Short('h')

	KP.Flag("app.baseurl", "App base url").Envar("BASE_URL").Default("").StringVar(&BaseURL)

	KP.Flag("env", "Environment").Envar("env").Default("prod").StringVar(&config.env)
}
