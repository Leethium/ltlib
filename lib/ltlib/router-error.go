package ltlib

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type ltlibRouterError struct {
	Err        string `json:"msg"`
	Code       uint   `json:"code"`
	HTTPCode   int    `json:"-"`
	Additional string `json:"additional"`
}

func (terr ltlibRouterError) Error() string {
	return fmt.Sprintf("HTTP=%d,ERR=%s,CODE=%d,ADD=%s", terr.HTTPCode, terr.Err, terr.Code, terr.Additional)
}

type ltlibRouterParseWrapper struct {
	Error ltlibRouterError `json:"error"`
}

func ParseError(resp *http.Response) error {
	if resp.StatusCode == 200 || resp.StatusCode == 201 || resp.StatusCode == 202 || resp.StatusCode == 207 {
		return nil
	}

	out := ltlibRouterParseWrapper{}
	err := json.NewDecoder(resp.Body).Decode(&out)
	if err != nil {
		return err
	}
	return out.Error
}
