package ltlib

import (
	"flag"
	"os"
	"path/filepath"
)

func init() {
	MigrateFolder = filepath.Join(".", MigrateFolder)
	Hooks.Add("ltlib.InitOnce", "ltlibInit", ltlibInit)
}

func ltlibInit(...any) {
	var err error

	if flag.Lookup("test.v") == nil {
		_, err = KP.Parse(os.Args[1:])
	} else {
		Warn("msg", "Running in test mode, command line args will be discarded")
		_, err = KP.Parse([]string{})
	}

	if err != nil {
		Error("msg", "Error parsing command line arguments", "error", err)
		KP.Usage(os.Args[1:])
		os.Exit(2)
	}

	if config.env == "prod" {
		Config.Prod = true
	}

	Hooks.RunAndClear("ltlib.PostInit")

	Info("msg", "initialisation done", "prod", Config.Prod)
}

// Init has to be run after settings kp flags
func Init() {
	Hooks.RunAndClear("ltlib.InitOnce")
}
