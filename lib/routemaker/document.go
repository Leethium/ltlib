package routemaker

import (
	"strings"
	"sync"

	"gitlab.com/leethium/ltlib/lib/ltlib"
	"gitlab.com/leethium/ltlib/lib/openapi"
)

type DocumentableStructSchema interface {
	WithOpenapiSchema() *openapi.Schema
}

var docLock sync.Mutex
var documentation *openapi.OpenApiDoc
var documentableOPStack = []func(*openapi.OpenApiDoc) *openapi.Operation{}
var documentableStack = map[Method]map[string][]Documentable{}

func normalizePath(path string) (string, []string, string) {
	catchAll := ""
	params := []string{}
	pathSpl := strings.Split(path, "/")
	for k, v := range pathSpl {
		if k == 0 {
			continue
		}
		if v[0] == ':' || v[0] == '*' {
			params = append(params, v[1:])
			pathSpl[k] = "{" + v[1:] + "}"
			if v[0] == '*' {
				catchAll = v[1:]
			}
		}
	}
	return strings.Join(pathSpl, "/"), params, catchAll
}

func generateOperationId(method Method, path string) string {
	pathSpl := strings.Split(path[1:], "/")
	for k, v := range pathSpl {
		if len(v) <= 1 {
			pathSpl[k] = strings.ToUpper(v)
		}
		pathSpl[k] = strings.ToUpper(string([]byte{v[0]})) + v[1:]
	}
	return strings.ToLower(string(method)) + strings.Join(pathSpl, "")
}

func makeOperation(method Method, path string, doc *openapi.OpenApiDoc) *openapi.Operation {
	opPath, params, catchAll := normalizePath(path)
	var docPath map[openapi.HttpMethod]*openapi.Operation
	var docOp *openapi.Operation
	var ok bool
	if docPath, ok = doc.Paths[openapi.HttpPath(opPath)]; !ok {
		docPath = map[openapi.HttpMethod]*openapi.Operation{}
		doc.Paths[openapi.HttpPath(opPath)] = docPath
	}

	if docOp, ok = docPath[method.AsOpenAPIMethod()]; !ok {
		docOp = &openapi.Operation{}
		doc.Paths[openapi.HttpPath(opPath)][method.AsOpenAPIMethod()] = docOp
	}
	if docOp.OperationId == "" {
		docOp.OperationId = generateOperationId(method, path)
	}

	if len(docOp.Parameters) == 0 {
		docOp.Parameters = []*openapi.Parameter{}
	}

	for _, param := range params {
		found := false
		var targetDocParam = &openapi.Parameter{}
		for _, docParam := range docOp.Parameters {
			if docParam.In == "path" && docParam.Name == param {
				targetDocParam = docParam
				found = true
				break
			}
		}
		targetDocParam.In = "path"
		targetDocParam.Name = param
		targetDocParam.Required = true
		schema := targetDocParam.Schema
		if schema == nil {
			schema = &openapi.Schema{}
			targetDocParam.Schema = schema
		}
		if schema.Type == "" {
			schema.Type = "string"
		}
		if catchAll == param && schema.Format == "" {
			schema.Format = "path"
		}
		if !found {
			docOp.Parameters = append(docOp.Parameters, targetDocParam)
		}
	}

	return docOp
}

func handleDocumentablePath(method Method, path string) {
	docLock.Lock()
	defer docLock.Unlock()
	documentableOPStack = append(documentableOPStack, func(oad *openapi.OpenApiDoc) *openapi.Operation {
		return makeOperation(method, path, documentation)
	})
	if documentation != nil {
		makeOperation(method, path, documentation)
	}
}

func handleGlobalDocInfos(*openapi.OpenApiDoc) {
	// Server ip ? / API Path ? / Version ? / Revision ?
}

func mustGetOperationByPath(method Method, path string) *openapi.Operation {
	var docPath *openapi.Operation
	if docPathMeth, ok := documentation.Paths[openapi.HttpPath(path)]; !ok {
		ltlib.Fatal("msg", "MustGetDocumentablePath failed", "path", path, "inDocumentationPath", openapi.HttpPath(path))
	} else if docPath, ok = docPathMeth[method.AsOpenAPIMethod()]; !ok {
		ltlib.Fatal("msg", "MustGetDocumentablePath failed on method", "path", path, "inDocumentationPath", openapi.HttpPath(path))
	}

	if docPath == nil {
		ltlib.Fatal("msg", "docPath is nil", "path", path, "inDocumentationPath", openapi.HttpPath(path))
	}

	return docPath
}

func handleDocumentableProvider[T any](method Method, path string, provider Provider[T]) {
	if asDocumentable, ok := provider.(Documentable); ok {
		docLock.Lock()
		defer docLock.Unlock()
		if _, ok := documentableStack[method]; !ok {
			documentableStack[method] = map[string][]Documentable{}
		}
		if _, ok := documentableStack[method][path]; !ok {
			documentableStack[method][path] = []Documentable{}
		}
		documentableStack[method][path] = append(documentableStack[method][path], asDocumentable)
		if documentation != nil {
			err := asDocumentable.ApplyDocumentation(documentation, mustGetOperationByPath(method, path))
			ltlib.FatalIf(err)
		}
	}
}
