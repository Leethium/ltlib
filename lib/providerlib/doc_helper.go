package providerlib

import (
	"gitlab.com/leethium/ltlib/lib/openapi"
)

func ApplyDocumentationByRef(refname string, contentType string, respCode openapi.ResponseCode, doc *openapi.OpenApiDoc, op *openapi.Operation) {
	ref := string("#/components/schemas/" + refname)
	if op.Responses == nil {
		op.Responses = make(map[openapi.ResponseCode]*openapi.Response)
	}
	resps, ok := op.Responses[respCode]
	if !ok {
		resps = &openapi.Response{
			Content: map[string]*openapi.MediaType{},
		}
		op.Responses[respCode] = resps
	}

	var curResp *openapi.Schema
	if resps.Content == nil {
		resps.Content = make(map[string]*openapi.MediaType)
	}

	if tmp, ok := resps.Content[contentType]; ok {
		curResp = tmp.Schema
	} else {
		curResp = &openapi.Schema{}
		op.Responses[respCode].Content[contentType] = &openapi.MediaType{
			Schema: curResp,
		}
	}

	curResp = &openapi.Schema{}
	resps.Content[contentType] = &openapi.MediaType{
		Schema: curResp,
	}

	if curResp.IsEmpty() {
		curResp.AnyOf = []*openapi.Schema{
			{REF: ref},
		}
		return
	} else {
		if len(curResp.AnyOf) > 0 {
			for _, v := range curResp.AnyOf {
				if v.REF == ref {
					return
				}
			}
			curResp.AnyOf = append(curResp.AnyOf, &openapi.Schema{REF: ref})
			return
		}

		oldSchema := curResp
		curResp = &openapi.Schema{
			AnyOf: []*openapi.Schema{
				oldSchema,
				{
					REF: ref,
				},
			},
		}
	}
}

func GenerateBasicBodyDocRef(componentname string, description string, dataSchema *openapi.Schema, example *HttpGenericJSONResponse, doc *openapi.OpenApiDoc) {
	componentName := openapi.ComponentName(componentname)
	doc.Components.Schemas[componentName] = dataSchema
}

func GenerateBasicRespDocRef(componentname string, description string, dataSchema *openapi.Schema, example *HttpGenericJSONResponse, doc *openapi.OpenApiDoc) {
	componentName := openapi.ComponentName(componentname)

	curComponent, ok := doc.Components.Schemas[componentName]
	if !ok {
		curComponent = &openapi.Schema{}
		doc.Components.Schemas[componentName] = curComponent
	}

	// curComponent.Example
	curComponent.Description = description
	curComponent.Type = "object"
	curComponent.Required = []string{"success"}

	if example != nil && example.Error != nil {
		curComponent.Required = []string{"success", "error"}
	}
	if dataSchema != nil {
		curComponent.Required = []string{"success", "data"}
	}

	if example != nil {
		curComponent.Example = example
	}

	curComponent.Properties = map[string]*openapi.Schema{
		"success": {
			Type: "boolean",
		},
		"data": {
			Type: "object",
		},
		"data_type": {
			Type: "string",
		},
		"skip": {
			Type: "integer",
		},
		"count": {
			Type: "integer",
		},
		"total": {
			Type: "integer",
		},
		"cursor": {
			Type: "integer",
		},
		"next": {
			Type: "boolean",
		},
		"error": {
			Type: "object",
			Properties: map[string]*openapi.Schema{
				"code": {
					Type: "integer",
				},
				"msg": {
					Type: "string",
				},
				"additional": {
					Type: "string",
				},
			},
		},
	}
	if dataSchema != nil {
		curComponent.Properties["data"] = dataSchema
	}
	if example != nil && example.Error != nil {
		curComponent.Properties["error"].Required = []string{"code", "msg"}
	}

}
