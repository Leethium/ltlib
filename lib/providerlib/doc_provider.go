package providerlib

import (
	"net/http"

	"gitlab.com/leethium/ltlib/lib/openapi"
	"gitlab.com/leethium/ltlib/lib/routemaker"
)

type DocProvider struct {
	Override func(*openapi.OpenApiDoc, *openapi.Operation) error
}

func (dp *DocProvider) Provide(resp http.ResponseWriter, req *http.Request, prevProvided []any) (output any, next bool) {
	return nil, true
}

func (dp *DocProvider) ApplyDocumentation(doc *openapi.OpenApiDoc, op *openapi.Operation) error {
	return dp.Override(doc, op)
}

func ProviderSimpleDoc(summary string, tags []*openapi.Tag) routemaker.Provider[any] {
	return &DocProvider{
		Override: func(doc *openapi.OpenApiDoc, op *openapi.Operation) error {
			op.Summary = summary

			for _, v := range tags {
				foundDoc := false
				foundOp := false
				for _, w := range doc.Tags {
					if w.Name == v.Name {
						foundDoc = true
					}
				}
				if !foundDoc {
					doc.Tags = append(doc.Tags, v)
				}

				for _, w := range op.Tags {
					if v.Name == w {
						foundOp = true
						break
					}
				}
				if !foundOp {
					op.Tags = append(op.Tags, v.Name)
				}
			}
			return nil
		},
	}
}
