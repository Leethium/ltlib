package routemaker

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

var ParamsProvider = &ParamsProviderImpl{}

type ParamsProviderImpl struct{}

func (*ParamsProviderImpl) Provide(resp http.ResponseWriter, req *http.Request, prevProvided []any) (output httprouter.Params, next bool) {
	return prevProvided[0].(httprouter.Params), true
}

// ParamsUniqueProvider
// ParamsMultipleProvider => Struct w/ annotations
