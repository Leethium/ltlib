package ltlib

import (
	"time"

	"github.com/go-sql-driver/mysql" // Requires mysql
	"github.com/jmoiron/sqlx"
)

var DB *sqlx.DB

var mysqldbcfg = mysql.NewConfig()

var mySQLConfig struct {
	DBHost     string
	DBUser     string
	DBPassword string
	DBName     string
}

func init() {
	KP.Flag("db.host", "MySQL Database DSN address").Envar("DB_URL").StringVar(&mySQLConfig.DBHost)
	KP.Flag("db.user", "MySQL Database username").Envar("DB_USER").StringVar(&mySQLConfig.DBUser)
	KP.Flag("db.pass", "MySQL Database password").Envar("DB_PASSWORD").StringVar(&mySQLConfig.DBPassword)
	KP.Flag("db.name", "MySQL Database name").Envar("DB_NAME").StringVar(&mySQLConfig.DBName)

	Hooks.Add("ltlib.PostInit", "initDatabase", initDb)
}

func initDb(...any) {
	if mySQLConfig.DBHost == "" {
		Warn("msg", "Mysql DB Host not set, skipping mysql initialisation")
		return
	}
	var err error
	mysqldbcfg.User = mySQLConfig.DBUser
	mysqldbcfg.Passwd = mySQLConfig.DBPassword
	mysqldbcfg.DBName = mySQLConfig.DBName
	mysqldbcfg.Addr = mySQLConfig.DBHost
	mysqldbcfg.ParseTime = true
	mysqldbcfg.Net = "tcp"
	mysqldbcfg.Collation = "utf8_general_ci"

	DB, err = sqlx.Open("mysql", mysqldbcfg.FormatDSN())
	DB.DB.SetConnMaxIdleTime(120 * time.Second)
	FatalIf(err)
	err = DB.Ping()
	FatalIf(err)
	Hooks.RunAndClear("ltlib.PostInitMySQLDatabase")
}
