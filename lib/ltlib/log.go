package ltlib

import (
	"os"
	"runtime/debug"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

var hlog log.Logger

// applyLogLevel applies min logging level
var applyLogLevel func(string)
var logLevel string

func init() {
	KP.Flag("log.level", "Log level (all, debug, info, warn, error)").Envar("LOG_LEVEL").Default("info").StringVar(&logLevel)

	Hooks.Add("ltlib.PostInit", "initLogger", func(a ...any) {
		applyLogLevel(logLevel)
	})

	logger := log.NewLogfmtLogger(os.Stdout)
	hlog = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.Caller(4))

	applyLogLevel = func(lvl string) {
		switch lvl {
		case "debug":
			hlog = level.NewFilter(hlog, level.AllowDebug())
		case "warn":
			hlog = level.NewFilter(hlog, level.AllowWarn())
		case "error":
			hlog = level.NewFilter(hlog, level.AllowError())
		case "all":
			hlog = level.NewFilter(hlog, level.AllowAll())
		default:
			hlog = level.NewFilter(hlog, level.AllowInfo())
		}
		Info("msg", "Log level set", "level", lvl)
		applyLogLevel = func(string) {}
	}
}

// Debug add a log entry w/ Debug level
func Debug(keyvals ...interface{}) {
	level.Debug(hlog).Log(keyvals...)
}

// Info add a log entry w/ Info level
func Info(keyvals ...interface{}) {
	level.Info(hlog).Log(keyvals...)
}

// Warn add a log entry w/ Warn level
func Warn(keyvals ...interface{}) {
	level.Warn(hlog).Log(keyvals...)
}

// Error add a log entry w/ Error level
func Error(keyvals ...interface{}) {
	level.Error(hlog).Log(keyvals...)
	Hooks.Run("log.caughtError", keyvals...)
}

// Fatal add a log entry w/ Error level and exits
func Fatal(keyvals ...interface{}) {
	debug.PrintStack()
	Hooks.Run("log.caughtFatal", keyvals...)
	level.Error(hlog).Log(keyvals...)
	os.Exit(1)
}

// FatalIf prints a fatal Error level and exits if err != nil
func FatalIf(err error) {
	if err == nil {
		return
	}
	Hooks.Run("log.caughtFatal", "err", err)
	level.Error(hlog).Log("err", err)
	os.Exit(1)
}
