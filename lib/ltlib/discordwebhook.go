package ltlib

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
)

type DiscordWebhookParams struct {
	Content  string                 `json:"content,omitempty"`
	Username string                 `json:"username,omitempty"`
	Embeds   []*DiscordMessageEmbed `json:"embeds,omitempty"`
}
type DiscordMessageEmbed struct {
	URL         string                     `json:"url,omitempty"`
	Type        string                     `json:"type,omitempty"`
	Title       string                     `json:"title,omitempty"`
	Description string                     `json:"description,omitempty"`
	Timestamp   string                     `json:"timestamp,omitempty"`
	Color       int                        `json:"color,omitempty"`
	Footer      *DiscordMessageEmbedFooter `json:"footer,omitempty"`
}
type DiscordMessageEmbedFooter struct {
	Text string `json:"text,omitempty"`
}

var discordWebhookUrl = ""

func init() {
	KP.Flag("discord.webhook", "Discord webhook url").Envar("DISCORD_WEBHOOK").StringVar(&discordWebhookUrl)
}

func (emb *DiscordWebhookParams) Send() error {
	if discordWebhookUrl == "" {
		return errors.New("WebhookURL Not defined")
	}
	bt, _ := json.Marshal(emb)
	_, err := http.Post(discordWebhookUrl, "application/json", bytes.NewReader(bt))
	return err
}
