package ltlib

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"time"
)

type HtmlPdfAPIStub struct {
}

func (*HtmlPdfAPIStub) HtmlToPDF(string) (io.Reader, error) {
	return nil, errors.New("HTMLConvApi not initialized")
}

type HtmlPdfHandler interface {
	HtmlToPDF(string) (io.Reader, error)
}

var HTMLConvApi HtmlPdfHandler = &HtmlPdfAPIStub{}

type htmlPdfApi struct {
	xAuthToken string
	dpi        int
	apiURL     string
	cli        http.Client
}

var htmlPdfApiConf = &htmlPdfApi{
	dpi: 300,
	cli: http.Client{
		Timeout: time.Second * 30,
		Transport: &http.Transport{
			DisableKeepAlives: true, // FaaS, avoid paying for unused connexions
			MaxConnsPerHost:   5,
		},
	},
}

func init() {
	KP.Flag("pdfapi.url", "HtmlToPDF Api url").Envar("PDFAPI_URL").StringVar(&htmlPdfApiConf.apiURL)
	KP.Flag("pdfapi.key", "HtmlToPDF Api key (X-Auth-Token HEADER)").Envar("PDFAPI_KEY").StringVar(&htmlPdfApiConf.xAuthToken)
	Hooks.Add("ltlib.PostInit", "initPDFApi", initPDFApi)
}

func initPDFApi(...any) {
	if htmlPdfApiConf.apiURL == "" {
		Warn("msg", "PDF API URL not set, skipping HTMLConvApi initialisation")
		return
	}
	HTMLConvApi = htmlPdfApiConf
}

func (pdfapi *htmlPdfApi) HtmlToPDF(html string) (io.Reader, error) {
	bt, _ := json.Marshal(struct {
		DPI  int    `json:"dpi"`
		Html string `json:"html"`
	}{DPI: pdfapi.dpi, Html: html})

	req, _ := http.NewRequest("POST", pdfapi.apiURL+"/pdf", bytes.NewReader(bt))
	req.Header.Add("Content-Type", "application/json")
	if pdfapi.xAuthToken != "" {
		req.Header.Add("X-Auth-Token", pdfapi.xAuthToken)
	}
	resp, err := pdfapi.cli.Do(req)
	if err != nil {
		return nil, err
	}
	err = ParseError(resp)
	if err != nil {
		return nil, err
	}
	return resp.Body, nil
}
