package routemaker

import (
	"net/http"
)

type InheritProvider[T1 any, T2 any] struct {
	ProvideWithParent func(resp http.ResponseWriter, req *http.Request, data T1, prevProvided []any) (output T2, next bool)
}

func ProvideWithPrevious[T1 any, T2 any](cb func(resp http.ResponseWriter, req *http.Request, data T1, prevProvided []any) (T2, bool)) Provider[T2] {
	return &InheritProvider[T1, T2]{
		ProvideWithParent: cb,
	}
}

func (ip *InheritProvider[T1, T2]) Provide(resp http.ResponseWriter, req *http.Request, prevProvided []any) (output T2, next bool) {
	var cast T1
	var ok bool
	for _, v := range prevProvided {
		if cast, ok = v.(T1); ok {
			break
		}
	}
	if !ok {
		panic("InheritProvider requested and no parent type is present")
	}

	return ip.ProvideWithParent(resp, req, cast, prevProvided)
}
