package ltlib

import (
	"runtime/debug"
	"strconv"
	"time"
)

var CVSInfo = CVSInfoData{}

type CVSInfoData struct {
	Revision string
	Time     time.Time
	Modified bool
}

func init() {
	if info, ok := debug.ReadBuildInfo(); ok {
		for _, setting := range info.Settings {
			if CVSInfo.Revision == "" && setting.Key == "vcs.revision" {
				CVSInfo.Revision = setting.Value
			}
			if CVSInfo.Time.IsZero() && setting.Key == "vcs.time" {
				timestamp, _ := strconv.Atoi(setting.Value)
				CVSInfo.Time = time.Unix(int64(timestamp), 0)
			}
			if !CVSInfo.Modified && setting.Key == "vcs.modified" {
				if setting.Value == "true" {
					CVSInfo.Modified = true
				}
			}
		}
	}
}

// Ver returns version info
func Ver() string {
	ver := CVSInfo.Time.Format("2006-01-02-15:04:05") + "_" + CVSInfo.Revision
	if CVSInfo.Modified {
		ver += "_dirty"
	}
	return ver
}
