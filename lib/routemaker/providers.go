package routemaker

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/leethium/ltlib/lib/ltlib"
	"gitlab.com/leethium/ltlib/lib/openapi"
)

var BaseHandler = func(resp http.ResponseWriter, req *http.Request) {
	origin := ""
	if ltlib.Config.Prod {
		origin = ltlib.BaseURL
	} else {
		origin = req.Header.Get("origin")
		if origin == "" {
			origin = "http://localhost"
		}
	}
	resp.Header().Set("Access-Control-Allow-Origin", origin)
	resp.Header().Set("Access-Control-Allow-Credentials", "true")
	resp.Header().Set("Access-Control-Allow-Headers", req.Header.Get("Access-Control-Request-Headers"))
}

type Provider[T any] interface {
	Provide(resp http.ResponseWriter, req *http.Request, prevProvided []any) (output T, next bool)
}

type Documentable interface {
	ApplyDocumentation(*openapi.OpenApiDoc, *openapi.Operation) error
}

func HandleUndocumented(router *httprouter.Router, method Method, path string, cb func(resp http.ResponseWriter, req *http.Request)) {
	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)

		cb(w, r)
	})
}
