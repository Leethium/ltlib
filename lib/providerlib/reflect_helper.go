package providerlib

import (
	"encoding/json"
	"reflect"
	"strings"
	"time"

	"gitlab.com/leethium/ltlib/lib/ltlib"
	"gitlab.com/leethium/ltlib/lib/openapi"
	"gitlab.com/leethium/ltlib/lib/routemaker"
)

var JSONStructMaxDepth = 3

func ValueToSchema(v any) *openapi.Schema {
	out := valueToSchema(reflect.TypeOf(v), JSONStructMaxDepth)
	// In case we get a pointer at root level, make it non nullable
	out.Nullable = false
	return out
}

func valueToSchema(t reflect.Type, maxdepth int) *openapi.Schema {
	if maxdepth == 0 {
		return &openapi.Schema{
			Type: "object",
		}
	}
	maxdepth--

	if reflect.ValueOf(time.Time{}).Type() == t {
		return &openapi.Schema{
			Type:        "string",
			Description: "Golang DateTime as a RFC3339Nano string use date.toIsoString() in javascript",
			Example:     "2006-01-02T15:04:05.999999999Z07:00",
		}
	}

	if t.Implements(reflect.TypeOf((*routemaker.DocumentableStructSchema)(nil)).Elem()) {
		return reflect.New(t).Interface().(routemaker.DocumentableStructSchema).WithOpenapiSchema()
	} else if t.Implements(reflect.TypeOf((*json.Marshaler)(nil)).Elem()) {
		ltlib.Warn("msg", "type does implement a custom json.Marshaler but not routemaker.DocumentableStructSchema, swagger.yml might generate with a wrong schema for given type", "package", t.PkgPath(), "type", t)
	}

	switch t.Kind() {
	case reflect.Ptr:
		t = t.Elem()
		val := valueToSchema(t, maxdepth+1) // Reiterate with real value, and assume it's nullable
		val.Nullable = true
		return val
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return &openapi.Schema{
			Type: "integer",
		}
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return &openapi.Schema{
			Type:    "integer",
			Minimum: 0,
		}
	case reflect.Float32, reflect.Float64:
		return &openapi.Schema{
			Type: "number",
		}
	case reflect.String:
		return &openapi.Schema{
			Type: "string",
		}
	case reflect.Bool:
		return &openapi.Schema{
			Type: "boolean",
		}
	case reflect.Array, reflect.Slice:
		return &openapi.Schema{
			Type: "array",
			// Don't go deeper if it's an array
			// useful for not truncating multi-dimensional arrays
			Items: valueToSchema(t.Elem(), maxdepth+1),
		}
	case reflect.Map:
		return &openapi.Schema{
			Type: "object",
			// Don't go deeper if it's a map
			// useful for not truncating multi-dimensional maps
			AdditionalProperties: valueToSchema(t.Elem(), maxdepth+1),
		}
	case reflect.Interface:
		return &openapi.Schema{
			Type: "object",
		}
	case reflect.Struct:
		sc := &openapi.Schema{
			Type:       "object",
			Properties: map[string]*openapi.Schema{},
		}
		for i := 0; i < t.NumField(); i++ {
			structField := t.Field(i)

			if !structField.IsExported() {
				continue
			}

			fieldVal := t.Field(i).Type
			tagName := structField.Name
			optional := false
			if jsonTag, ok := structField.Tag.Lookup("json"); ok {
				t, opt := parseJsonTag(jsonTag)
				if t == "" {
					continue
				}
				if t == "-" {
					continue
				}
				tagName = t
				optional = opt
			}
			sc.Properties[tagName] = valueToSchema(fieldVal, maxdepth)
			if !optional {
				sc.Required = append(sc.Required, tagName)
			}
		}

		return sc
	default:
		ltlib.Fatal("msg", "can't work with kind", "kind", t.Kind())
	}

	return nil
}

func parseJsonTag(tag string) (out string, nullable bool) {
	tag, opt, _ := strings.Cut(tag, ",")
	if strings.Contains(opt, "omitempty") {
		return tag, true
	}
	return tag, false
}
