package routemaker

import (
	"strings"

	"gitlab.com/leethium/ltlib/lib/openapi"
)

type Method string

const (
	GET     Method = "GET"
	POST    Method = "POST"
	PUT     Method = "PUT"
	PATCH   Method = "PATCH"
	DELETE  Method = "DELETE"
	OPTIONS Method = "OPTIONS"
)

func (m Method) AsOpenAPIMethod() openapi.HttpMethod {
	return openapi.HttpMethod(strings.ToLower(string(m)))
}
