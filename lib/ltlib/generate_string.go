package ltlib

import (
	crand "crypto/rand"
	"encoding/binary"
	"encoding/hex"
	"log"
	"math/rand"
	"strings"

	"github.com/rs/xid"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var rnd rand.Source

func init() {
	rnd = rand.New(crandSrc{})
}

type crandSrc struct{}

func (s crandSrc) Seed(seed int64) {}

func (s crandSrc) Int63() int64 {
	return int64(s.Uint64() & ^uint64(1<<63))
}

func (s crandSrc) Uint64() (v uint64) {
	err := binary.Read(crand.Reader, binary.BigEndian, &v)
	if err != nil {
		log.Fatal(err)
	}
	return v
}

// GenerateRandomString generates a cryptographicaly random string that matches [a-zA-Z]
func GenerateRandomString(n int) string {
	sb := strings.Builder{}
	sb.Grow(n)
	for i, cache, remain := n-1, rnd.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = rnd.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			sb.WriteByte(letterBytes[idx])
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return sb.String()
}

func GenerateHex(n int) string {
	bytes := make([]byte, n)
	if _, err := rand.Read(bytes); err != nil {
		Fatal("err", err)
	}
	return hex.EncodeToString(bytes)
}

func GenerateXid() string {
	return xid.New().String()
}
