package ltlib

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/hex"
	"io"
	"math/rand"
	"strings"

	"github.com/martinlindhe/base36"
)

var aesCipher cipher.Block
var randSeed int64
var psk = ""

func init() {
	Hooks.Add("ltlib.PostSQLMigrate", "initAESCBC", initAESCBC)
}

func initAESCBC(...any) {
	token := ""
	DB.Get(&token, "SELECT data FROM ltlibmeta WHERE id=?", "ltlib.AESCBCKey")
	if len(token) == 0 {
		Warn("msg", "ltlib.AESCBCKey not found, generating")
		psk = GenerateHex(32)
		DB.Exec("INSERT INTO ltlibmeta(id, data) VALUES (?, ?)", "ltlib.AESCBCKey", psk)
	} else {
		Info("msg", "ltlib.AESCBCKey loaded")
		psk = token
	}

	var err error
	key, err := hex.DecodeString(psk)
	FatalIf(err)
	var rd uint64
	for i := 0; i < 8; i++ {
		rd += uint64(key[i]) << (uint64(i) << 3)
	}
	randSeed = int64(rd)

	aesCipher, err = aes.NewCipher(key)
	FatalIf(err)
}

func EncryptAESCBC(text string) string {
	plaintext := []byte(text)

	// CBC mode works on blocks so plaintexts may need to be padded to the
	// next whole block. For an example of such padding, see
	// https://tools.ietf.org/html/rfc5246#section-6.2.3.2.

	var delta = aes.BlockSize - len(plaintext)%aes.BlockSize
	var app = make([]byte, delta)
	plaintext = append(plaintext, app...)

	// The IV needs to be unique through the process, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]

	if _, err := io.ReadFull(rand.New(rand.NewSource(randSeed)), iv); err != nil {
		FatalIf(err)
	}

	mode := cipher.NewCBCEncrypter(aesCipher, iv)
	mode.CryptBlocks(ciphertext[aes.BlockSize:], plaintext)

	// It's important to remember that ciphertexts must be authenticated
	// (i.e. by using crypto/hmac) as well as being encrypted in order to
	// be secure.

	return base36.EncodeBytes(ciphertext)
}

func DecryptAESCBC(text string) string {
	ciphertext := base36.DecodeToBytes(text)
	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(ciphertext) < aes.BlockSize {
		Fatal("msg", "ciphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	// CBC mode always works in whole blocks.
	if len(ciphertext)%aes.BlockSize != 0 {
		Fatal("msg", "ciphertext is not a multiple of the block size")
	}

	mode := cipher.NewCBCDecrypter(aesCipher, iv)

	// CryptBlocks can work in-place if the two arguments are the same.
	mode.CryptBlocks(ciphertext, ciphertext)

	// If the original plaintext lengths are not a multiple of the block
	// size, padding would have to be added when encrypting, which would be
	// removed at this point. For an example, see
	// https://tools.ietf.org/html/rfc5246#section-6.2.3.2. However, it's
	// critical to note that ciphertexts must be authenticated (i.e. by
	// using crypto/hmac) before being decrypted in order to avoid creating
	// a padding oracle.

	out := string(ciphertext)
	return strings.TrimRight(out, "")
}
