package ltlib

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"
)

const schema = `CREATE TABLE IF NOT EXISTS ltlibmeta(
	id VARCHAR(32) NOT NULL,
	data VARCHAR(1024) NOT NULL default '',
	PRIMARY KEY(id)
)`

// MigrateFolder defines the folder whare MYSQLdb migrate scripts are located
var MigrateFolder = "migrations"

// MigrateTimeFormat defines the timeformat as required in MYSQLdb migrate scripts
const MigrateTimeFormat = "2006-01-02-15-04"

func init() {
	Hooks.Add("ltlib.PostInitMySQLDatabase", "initMigrate", initMigrate)
}

func initMigrate(...any) {
	defer Hooks.RunAndClear("ltlib.PostSQLMigrate")

	_, err := DB.Exec(schema)
	FatalIf(err)

	var lock []int
	err = DB.Select(&lock, "SELECT GET_LOCK('migrate', 5);")
	FatalIf(err)
	if lock[0] == 0 {
		Fatal("Could not acquire lock")
	}
	defer DB.Exec("SELECT RELEASE_LOCK('migrate')")

	var lastMigrate []string
	lastMigrateDate := time.Time{}
	err = DB.Select(&lastMigrate, "SELECT data FROM ltlibmeta WHERE id=?", "lastMigrate")
	FatalIf(err)
	if len(lastMigrate) == 0 {
		Warn("msg", "no last migration date found")
	} else {
		var err error
		lastMigrateDate, err = time.Parse(MigrateTimeFormat, lastMigrate[0])
		FatalIf(err)
		Info("msg", "last migration date", "date", lastMigrateDate.Format(MigrateTimeFormat))
	}
	newTime := performMigration(lastMigrateDate)
	if newTime.Equal(time.Time{}) {
		return
	}
	_, err = DB.Exec("REPLACE INTO ltlibmeta(id, data) VALUES (?,?)", "lastMigrate", newTime.Format(MigrateTimeFormat))
	Info("msg", "Migrations done")
}

func performMigration(from time.Time) time.Time {
	dir, err := os.ReadDir(MigrateFolder)
	if err != nil {
		Error("msg", "no migrations found", "err", err)
		return time.Time{}
	}

	if len(dir) == 0 {
		Warn("msg", "no migrations in folder")
		return time.Time{}
	}

	// Filter
	{
		s2 := dir
		dir = dir[:0]
		for _, v := range s2 {
			if v.IsDir() {
				continue
			}
			if v.Name()[0] == '.' {
				continue
			}
			dir = append(dir, v)
		}
	}

	if len(dir) == 0 {
		Warn("msg", "No migrations in folder")
		return time.Time{}
	}

	sort.Sort(alphabetical(dir))

	// Skip already done
	{
		skip := 0
		for k, file := range dir {
			date := migrationDate(file.Name())
			if date.Equal(from) || date.Before(from) {
				skip = k + 1
				continue
			}
			break
		}
		dir = dir[skip:]
		if len(dir) == 0 {
			Info("msg", "no migrations required")
			return time.Time{}
		}
	}

	for _, file := range dir {
		fname := file.Name()
		var execs []string
		{
			fcont, err := os.ReadFile(filepath.Join(MigrateFolder, fname))
			if err != nil {
				FatalIf(err)
			}
			execs = strings.Split(string(fcont), "\n\n\n\n")
		}
		execLen := len(execs)
		for k, v := range execs {
			Info("msg", "migration", "file", fname, "status", fmt.Sprintf("%d/%d", k+1, execLen))
			_, err := DB.Exec(v)
			FatalIf(err)
		}
	}
	return migrationDate(dir[len(dir)-1].Name())
}

func migrationDate(in string) time.Time {
	split := strings.Split(in, "_")
	out, err := time.Parse(MigrateTimeFormat, split[0])
	FatalIf(err)
	return out
}

type alphabetical []fs.DirEntry

func (ap alphabetical) Len() int {
	return len(ap)
}

func (ap alphabetical) Less(i, j int) bool {
	return ap[i].Name() < ap[j].Name()
}

func (ap alphabetical) Swap(i, j int) {
	ap[i], ap[j] = ap[j], ap[i]
}
