package main

import (
	"os"
	"path/filepath"

	"gitlab.com/leethium/ltlib/lib/ltlib"
)

func main() {
	dir, _ := os.Getwd()
	ltlib.MigrateFolder = filepath.Join(dir, "migrations")
	ltlib.Init()
	os.Exit(0)
}
