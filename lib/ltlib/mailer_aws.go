package ltlib

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ses"
)

var awsses *ses.SES

type awsmailer struct{}

func init() {
	Hooks.Add("ltlib.PostAWSInit", "initMailerAWS", func(a ...any) {
		awsses = ses.New(session.New(), AWSCfg)
		if Mailer != nil {
			Fatal("msg", "Attempted to register multiple mailers")
		}
		Mailer = &awsmailer{}
		Info("msg", "Mailer initialised", "provider", "AWS SES")
	})
}

func (*awsmailer) Send(to string, subject string, html, text string) error {
	email := &ses.SendEmailInput{
		Destination: &ses.Destination{
			ToAddresses: aws.StringSlice([]string{to}),
		},
		Source: &mailSender,
		Message: &ses.Message{
			Subject: &ses.Content{
				Charset: &mailCharset,
				Data:    &subject,
			},
			Body: &ses.Body{
				Html: &ses.Content{
					Charset: &mailCharset,
					Data:    &html,
				},
				Text: &ses.Content{
					Charset: &mailCharset,
					Data:    &text,
				},
			},
		},
	}

	if mailBCC != nil && len(mailBCC) != 0 && mailBCC[0] != "" {
		email.Destination.BccAddresses = aws.StringSlice(mailBCC)
	}

	sendret, err := awsses.SendEmail(email)
	if err != nil {
		Error("msg", "mail", "subject", subject, "to", to, "err", err)
		return err
	}
	Info("msg", "mail sent", "subject", subject, "to", to, "msgid", sendret.MessageId)
	return nil
}
