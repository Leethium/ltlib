package providerlib

import (
	"net/http"

	"gitlab.com/leethium/ltlib/lib/openapi"
	"gitlab.com/leethium/ltlib/lib/routemaker"
)

type BasicRespJson struct {
	HttpGenericJSONResponse

	resp http.ResponseWriter
	req  *http.Request
}

func (br *BasicRespJson) Send(data any) {
	br.Data = data
	if br.Error == nil {
		br.Success = true
	}
	br.SendTo(br.resp)
}

func (pp *BasicRespJson) Provide(resp http.ResponseWriter, req *http.Request, prevProvided []any) (output Sender[any], next bool) {
	return &BasicRespJson{
		resp: resp,
		req:  req,
	}, true
}

func (pp *BasicRespJson) ApplyDocumentation(doc *openapi.OpenApiDoc, op *openapi.Operation) error {
	compName := "respBasicJSON"

	GenerateBasicRespDocRef(
		compName,              // Component name
		"Basic json response", // Description
		nil,                   // Data Schema
		&HttpGenericJSONResponse{Data: map[string]any{}, Success: true}, // Example
		doc)

	ApplyDocumentationByRef(compName,
		"application/json",
		openapi.ToResponseCode(200),
		doc,
		op)

	return nil
}

func BasicRespJsonSender() routemaker.Provider[Sender[any]] {
	out := &BasicRespJson{}

	_ = routemaker.Documentable(out) // Ensure struct is documentable
	return out
}
