package ltlib

import "sync"

var Hooks = &HookEngine{table: map[string]map[string]func(...any){}}

type HookEngine struct {
	lock  sync.Mutex
	table map[string]map[string]func(...any)
}

func NewHookEngine() *HookEngine {
	return &HookEngine{
		table: map[string]map[string]func(...any){},
	}
}

func (he *HookEngine) Run(eventName string, vararg ...any) {
	if hooks, ok := he.table[eventName]; ok {
		for k, v := range hooks {
			Debug("event", eventName, "identifier", k, "msg", "triggered")
			v(vararg...)
			Debug("event", eventName, "identifier", k, "msg", "completed")
		}
		Debug("event", eventName, "msg", "completed")
	}
}

func (he *HookEngine) RunAndClear(eventName string, vararg ...any) {
	if hooks, ok := he.table[eventName]; ok {
		for k, v := range hooks {
			Debug("event", eventName, "identifier", k, "msg", "triggered")
			v(vararg...)
			Debug("event", eventName, "identifier", k, "msg", "completed")
		}
		he.lock.Lock()
		delete(he.table, eventName)
		he.lock.Unlock()
		Debug("event", eventName, "msg", "completed")
	}
}

func (he *HookEngine) Remove(eventName string, identifier string) {
	if _, ok := he.table[eventName]; ok {
		delete(he.table[eventName], identifier)
	}
}

func (he *HookEngine) Add(eventName string, identifier string, fn func(...any)) {
	if _, ok := he.table[eventName]; ok {
		if _, ok := he.table[eventName][identifier]; ok {
			panic("Duplicate hook identifier")
		} else {
			he.table[eventName][identifier] = fn
		}
	} else {
		he.table[eventName] = map[string]func(...any){
			identifier: fn,
		}
	}
}
