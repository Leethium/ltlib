package providerlib

type Sender[T any] interface {
	Send(T)
}
