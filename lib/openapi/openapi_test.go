package openapi_test

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/leethium/ltlib/lib/openapi"
	"gopkg.in/yaml.v3"
)

func TestSchema(t *testing.T) {
	source := []string{
		"https://raw.githubusercontent.com/OAI/OpenAPI-Specification/3.1.0/examples/v3.0/api-with-examples.yaml",
		"https://raw.githubusercontent.com/OAI/OpenAPI-Specification/3.1.0/examples/v3.0/petstore.yaml",
		"https://raw.githubusercontent.com/OAI/OpenAPI-Specification/3.1.0/examples/v3.0/petstore-expanded.yaml",
		"https://raw.githubusercontent.com/OAI/OpenAPI-Specification/3.1.0/examples/v3.0/link-example.yaml",
		"https://raw.githubusercontent.com/OAI/OpenAPI-Specification/3.1.0/examples/v3.0/uspto.yaml",
		"https://bread.toastate.io/swagger-myapi-dev.yml",
	}

	for _, v := range source {
		runTestSchema(v, t)
	}
}

func runTestSchema(source string, t *testing.T) {

	a := &openapi.OpenApiDoc{}
	data, _ := http.Get(source)
	schema, _ := ioutil.ReadAll(data.Body)
	ysd := yaml.NewDecoder(bytes.NewReader(schema))
	ysd.KnownFields(true)
	err := ysd.Decode(a)
	if err != nil {
		t.Error(err)
	}
	buf := bytes.NewBufferString("")
	yenc := yaml.NewEncoder(buf)
	yenc.SetIndent(2)
	err = yenc.Encode(a)
	if err != nil {
		t.Error(err)
	}
	ioutil.WriteFile("out.yml", buf.Bytes(), 0644)
	ioutil.WriteFile("out.spew", []byte(spew.Sdump(a)), 0644)
}
