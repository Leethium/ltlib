package jsonhelpers

import (
	"strconv"
	"strings"
	"time"
)

type JSONTimeAsUnixNano time.Time

func (c *JSONTimeAsUnixNano) UnmarshalJSON(b []byte) error {
	value := strings.Trim(string(b), `"`) //get rid of "
	if value == "" || value == "null" {
		return nil
	}

	i, err := strconv.Atoi(value)
	if err != nil {
		return err
	}

	t := time.Unix(0, int64(i))

	*c = JSONTimeAsUnixNano(t) //set result using the pointer
	return nil
}

func (c JSONTimeAsUnixNano) MarshalJSON() ([]byte, error) {
	return []byte(strconv.FormatInt(time.Time(c).UnixNano(), 10)), nil
}

type JSONTimeAsUnixSec time.Time

func (c *JSONTimeAsUnixSec) UnmarshalJSON(b []byte) error {
	value := strings.Trim(string(b), `"`) //get rid of "
	if value == "" || value == "null" {
		return nil
	}

	i, err := strconv.Atoi(value)
	if err != nil {
		return err
	}

	t := time.Unix(int64(i), 0)

	*c = JSONTimeAsUnixSec(t) //set result using the pointer
	return nil
}

func (c JSONTimeAsUnixSec) MarshalJSON() ([]byte, error) {
	return []byte(strconv.FormatInt(time.Time(c).Unix(), 10)), nil
}

type JSONTimeAsUnixMicro time.Time

func (c *JSONTimeAsUnixMicro) UnmarshalJSON(b []byte) error {
	value := strings.Trim(string(b), `"`) //get rid of "
	if value == "" || value == "null" {
		return nil
	}

	i, err := strconv.Atoi(value)
	if err != nil {
		return err
	}

	t := time.Unix(0, int64(i)*1000)

	*c = JSONTimeAsUnixMicro(t) //set result using the pointer
	return nil
}

func (c JSONTimeAsUnixMicro) MarshalJSON() ([]byte, error) {
	return []byte(strconv.FormatInt(time.Time(c).UnixMicro(), 10)), nil
}
