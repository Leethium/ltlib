package providerlib

import (
	"encoding/json"
	"net/http"
)

type HttpGenericJSONResponse struct {
	Success  bool     `json:"success"`
	Error    *HTTPErr `json:"error,omitempty"`
	Data     any      `json:"data,omitempty"`
	DataType string   `json:"data_type,omitempty"`

	// Results
	ResCursor  int   `json:"cursor,omitempty"`
	ResSkip    int   `json:"skip,omitempty"`
	ResCount   int   `json:"count,omitempty"`
	ResTotal   int   `json:"total,omitempty"`
	ResHasNext *bool `json:"next,omitempty"`
}

func (h *HttpGenericJSONResponse) SendTo(resp http.ResponseWriter) {
	jse := json.NewEncoder(resp)
	jse.SetEscapeHTML(false)
	jse.SetIndent("", "  ")
	jse.Encode(h)
}
