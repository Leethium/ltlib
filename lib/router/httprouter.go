package router

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/leethium/ltlib/lib/ltlib"
)

// DefRouter is the default exposed router
var Router *httprouter.Router

func init() {
	Router = httprouter.New()
	Router.GlobalOPTIONS = &HttpDefaultOptionsHandler{}
	Router.PanicHandler = HttpDefaultPanicHandler
}

var HttpDefaultPanicHandler = func(w http.ResponseWriter, req *http.Request, panicsource interface{}) {
	ltlib.Hooks.Run("ltlib.OnHttpPanic", req, panicsource)
	ltlib.Error("msg", "caught panic", "path", req.URL.Path, "method", req.Method)
}

type HttpDefaultOptionsHandler struct{}

func (*HttpDefaultOptionsHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	ltlib.Debug("type", "preflight", "url", req.URL)
	w.Header().Set("Allow", "GET, POST, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE")
	if ltlib.Config.Prod {
		w.Header().Set("Access-Control-Allow-Origin", ltlib.BaseURL)
	} else {
		origin := req.Header.Get("origin")
		if origin == "" {
			origin = "http://localhost"
		}
		w.Header().Set("Access-Control-Allow-Origin", req.Header.Get("origin"))
	}
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Headers", req.Header.Get("Access-Control-Request-Headers"))
}

var HttpCORSWrapper = func(cb func(http.ResponseWriter, *http.Request, httprouter.Params)) func(http.ResponseWriter, *http.Request, httprouter.Params) {
	return func(resp http.ResponseWriter, req *http.Request, params httprouter.Params) {
		ltlib.Debug("path", req.URL.Path, "method", req.Method)
		resp.Header().Set("Allow", "OPTIONS, GET, POST, PUT, DELETE")
		if ltlib.Config.Prod {
			resp.Header().Set("Access-Control-Allow-Origin", ltlib.BaseURL)
		} else {
			origin := req.Header.Get("origin")
			if origin == "" {
				origin = "http://localhost"
			}
			resp.Header().Set("Access-Control-Allow-Origin", req.Header.Get("origin"))
		}
		resp.Header().Set("Access-Control-Allow-Credentials", "true")
		resp.Header().Set("Access-Control-Allow-Headers", req.Header.Get("Access-Control-Request-Headers"))
		cb(resp, req, params)
	}
}
