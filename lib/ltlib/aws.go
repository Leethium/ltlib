package ltlib

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

var awsKey = ""
var awsSecret = ""
var awsRegion = ""
var awsS3Key = ""
var awsS3Secret = ""
var awsS3Endpoint = ""
var awsS3Region = ""
var AWSCfg *aws.Config

var AWSPrefix string
var AWSS3Bucket = ""
var AWSS3 *s3.S3

func init() {
	KP.Flag("aws.key", "AWS Key").Envar("AWS_KEY").Default("").StringVar(&awsKey)
	KP.Flag("aws.secret", "AWS Secret key").Envar("AWS_SECRET").Default("").StringVar(&awsSecret)
	KP.Flag("aws.region", "AWS Region").Envar("AWS_REGION").Default("eu-west-1").StringVar(&awsRegion)
	KP.Flag("aws.s3.key", "AWS S3 Key").Envar("AWS_S3_KEY").Default("").StringVar(&awsS3Key)
	KP.Flag("aws.s3.secret", "AWS S3 Secret key").Envar("AWS_S3_SECRET").Default("").StringVar(&awsS3Secret)
	KP.Flag("aws.s3.endpoint", "AWS S3 Endpoint").Envar("AWS_S3_ENDPOINT").StringVar(&awsS3Endpoint)
	KP.Flag("aws.s3.region", "AWS S3 region").Envar("AWS_S3_REGION").Default("eu-west-1").StringVar(&awsS3Region)
	KP.Flag("aws.s3.bucket", "AWS S3 S3 Bucket").Envar("AWS_S3_BUCKET").Default("").StringVar(&AWSS3Bucket)
	Hooks.Add("ltlib.PostInit", "initAWSses", initAWSses)
	Hooks.Add("ltlib.PostInit", "initAWSs3", initAWSs3)
}

func initAWSses(...any) {
	if awsKey == "" || awsSecret == "" {
		Warn("msg", "AWS Key not set, skipping AWS initialisation")
		return
	}
	AWSCfg = aws.NewConfig().WithRegion(awsRegion).WithCredentials(credentials.NewStaticCredentials(awsKey, awsSecret, ""))
	Hooks.RunAndClear("ltlib.PostAWSInit")
}

func initAWSs3(...any) {
	var s3config *aws.Config

	if AWSS3Bucket == "" {
		Warn("msg", "AWS S3 Bucket not set, skipping AWS S3 initialisation")
		return
	}

	if awsS3Key != "" {
		s3config = aws.NewConfig().WithRegion(awsS3Region).WithCredentials(credentials.NewStaticCredentials(awsS3Key, awsS3Secret, ""))
		if awsS3Endpoint != "" {
			s3config = s3config.WithEndpoint(awsS3Endpoint)
		}
	} else {
		s3config = AWSCfg
	}

	AWSS3 = s3.New(session.New(), s3config)
	Hooks.RunAndClear("ltlib.PostAWSS3Conf", s3config)
}
