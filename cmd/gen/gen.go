package main

import (
	"os"
	"strconv"
	"strings"
)

func main() {
	out := &strings.Builder{}

	out.WriteString(`package routemaker

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func Handle0(router *httprouter.Router, method Method, path string, cb func(resp http.ResponseWriter, req *http.Request), pany ...Provider[any]) {
	handleDocumentablePath(method, path)
	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		BaseHandler(w, r)
		for _,prov := range pany{
			_, next := prov.Provide(w, r, []any{p})
			if !next{
				return
			}
		}
		cb(w, r)
	})
}
`)

	for k := 1; k <= 20; k++ {
		out.WriteString("\n\n")
		out.WriteString("func Handle")
		out.WriteString(strconv.Itoa(k))
		out.WriteString("[")
		for l := 1; l <= k; l++ {
			out.WriteString("T")
			out.WriteString(strconv.Itoa(l))
			if l == k {
				out.WriteString(" any]")
			} else {
				out.WriteString(", ")
			}
		}

		out.WriteString("(router *httprouter.Router, method Method, path string, ")
		for l := 1; l <= k; l++ {
			out.WriteString("p")
			out.WriteString(strconv.Itoa(l))
			out.WriteString(" Provider[T")
			out.WriteString(strconv.Itoa(l))
			out.WriteString("], ")
		}
		out.WriteString("cb func(http.ResponseWriter, *http.Request, ")
		for l := 1; l <= k; l++ {
			out.WriteString("T")
			out.WriteString(strconv.Itoa(l))
			if l != k {
				out.WriteString(", ")
			}
		}
		out.WriteString("), pany ...Provider[any]) {\n")
		out.WriteString("	handleDocumentablePath(method, path)\n")
		for l := 1; l <= k; l++ {
			out.WriteString("	handleDocumentableProvider(method, path, p")
			out.WriteString(strconv.Itoa(l))
			out.WriteString(")\n")
		}
		out.WriteString("\n")
		out.WriteString("	router.Handle(string(method), path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {\n")
		out.WriteString("		BaseHandler(w, r)\n")
		l := 1
		for ; l <= k; l++ {
			out.WriteString("		r")
			out.WriteString(strconv.Itoa(l))
			out.WriteString(", next := p")
			out.WriteString(strconv.Itoa(l))
			out.WriteString(".Provide(w, r, []any{p")
			for m := 1; m < l; m++ {
				out.WriteString(", r")
				out.WriteString(strconv.Itoa(m))
			}
			out.WriteString("})\n")
			out.WriteString("		if !next {\n")
			out.WriteString("			return\n")
			out.WriteString("		}\n")
		}
		/*
			for _,prov := range pany{
				_, next := prov.Provide.Provide(w, r, []any{})
				if !next{
					return
				}
			}
		*/
		out.WriteString("		for _,prov := range pany{\n")
		out.WriteString("			_, next := prov.Provide(w, r, []any{p")
		for m := 1; m < l-1; m++ {
			out.WriteString(", r")
			out.WriteString(strconv.Itoa(m))
		}
		out.WriteString("})\n")
		out.WriteString("			if !next{\n")
		out.WriteString("				return\n")
		out.WriteString("			}\n")
		out.WriteString("		}\n\n")

		out.WriteString("		cb(w, r")
		for l := 1; l <= k; l++ {
			out.WriteString(", r")
			out.WriteString(strconv.Itoa(l))
		}
		out.WriteString(")\n")
		out.WriteString("	})\n")
		out.WriteString("}\n")
	}

	os.Stdout.WriteString(out.String())
}
