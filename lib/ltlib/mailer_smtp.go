package ltlib

import (
	"bufio"
	"bytes"
	"crypto/rand"
	"crypto/tls"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"math"
	"math/big"
	"mime"
	"mime/multipart"
	"mime/quotedprintable"
	"net/mail"
	"net/smtp"
	"net/textproto"
	"os"
	"path/filepath"
	"strings"
	"time"
	"unicode"
)

type smtpmailer struct {
	smtpAuth smtp.Auth
}

var smtpConfig struct {
	host string
	user string
	pass string
}

func init() {
	KP.Flag("smtp.host", "SMTP address:port").Envar("SMTP_HOST").Default("").StringVar(&smtpConfig.host)
	KP.Flag("smtp.user", "SMTP username").Envar("SMTP_USER").Default("").StringVar(&smtpConfig.user)
	KP.Flag("smtp.pass", "SMTP password").Envar("SMTP_PASSWORD").Default("").StringVar(&smtpConfig.pass)

	Hooks.Add("ltlib.PostInit", "initMailerSMTP", func(a ...any) {
		if smtpConfig.host == "" {
			return
		}
		spl := strings.Split(smtpConfig.host, ":")
		addrWOPort := ""
		if len(spl) > 0 {
			addrWOPort = spl[0]
		}
		if Mailer != nil {
			Fatal("msg", "Attempted to register multiple mailers")
		}
		Mailer = &smtpmailer{
			smtpAuth: smtp.PlainAuth("", smtpConfig.user, smtpConfig.pass, addrWOPort),
		}
	})
}

func (m *smtpmailer) Send(to string, subject string, html, text string) error {
	email := NewSMTPEmail()
	email.From = mailSender
	email.To = []string{to}
	email.Subject = subject
	if mailBCC != nil && len(mailBCC) != 0 {
		email.Bcc = mailBCC
	}

	err := email.send(smtpConfig.host, m.smtpAuth)
	if err != nil {
		Error("msg", "mail", "subject", subject, "to", to, "err", err)
		return err
	}
	Info("msg", "mail sent", "subject", subject, "to", to)
	return nil
}

const (
	smtpMaxLineLen         = 76                             // RFC 2045
	smtpDefaultContentType = "text/plain; charset=us-ascii" // RFC 2045 5.2
)

type SMTPEmail struct {
	From        string
	To          []string
	Cc          []string
	Bcc         []string
	Subject     string
	Text        []byte
	HTML        []byte
	Headers     textproto.MIMEHeader
	Attachments []*Attachment
	ReadReceipt []string
}

// Multipart
type part struct {
	header textproto.MIMEHeader
	body   []byte
}

func NewSMTPEmail() *SMTPEmail {
	return &SMTPEmail{Headers: textproto.MIMEHeader{}}
}

type trimmerReader struct {
	rd io.Reader
}

func (tr trimmerReader) Read(buf []byte) (int, error) {
	n, err := tr.rd.Read(buf)
	t := bytes.TrimLeftFunc(buf[:n], unicode.IsSpace)
	n = copy(buf, t)
	return n, err
}

func NewEmailFromReader(r io.Reader) (*SMTPEmail, error) {
	e := NewSMTPEmail()
	s := trimmerReader{rd: r}
	tp := textproto.NewReader(bufio.NewReader(s))

	// main headers
	hdrs, err := tp.ReadMIMEHeader()
	if err != nil {
		return e, err
	}

	// subject, to, cc, bcc, from
	for h, v := range hdrs {
		switch {
		case h == "Subject":
			e.Subject = v[0]
			delete(hdrs, h)
		case h == "To":
			e.To = v
			delete(hdrs, h)
		case h == "Cc":
			e.Cc = v
			delete(hdrs, h)
		case h == "Bcc":
			e.Bcc = v
			delete(hdrs, h)
		case h == "From":
			e.From = v[0]
			delete(hdrs, h)
		}
	}
	e.Headers = hdrs
	body := tp.R

	// Mime parts
	ps, err := parseMIMEParts(e.Headers, body)
	if err != nil {
		return e, err
	}
	for _, p := range ps {
		if ct := p.header.Get("Content-Type"); ct == "" {
			return e, errors.New("Mime ent witout content type")
		}
		ct, _, err := mime.ParseMediaType(p.header.Get("Content-Type"))
		if err != nil {
			return e, err
		}
		switch {
		case ct == "text/plain":
			e.Text = p.body
		case ct == "text/html":
			e.HTML = p.body
		}
	}
	return e, nil
}

func parseMIMEParts(hs textproto.MIMEHeader, b io.Reader) ([]*part, error) {
	var ps []*part

	if _, ok := hs["Content-Type"]; !ok {
		hs.Set("Content-Type", smtpDefaultContentType)
	}

	ct, params, err := mime.ParseMediaType(hs.Get("Content-Type"))
	if err != nil {
		return ps, err
	}

	// Recursif pour multipart
	if strings.HasPrefix(ct, "multipart/") {
		if _, ok := params["boundary"]; !ok {
			return ps, errors.New("Multipart boundary")
		}
		mr := multipart.NewReader(b, params["boundary"])
		for {
			var buf bytes.Buffer
			p, err := mr.NextPart()
			if err == io.EOF {
				break
			}
			if err != nil {
				return ps, err
			}
			if _, ok := p.Header["Content-Type"]; !ok {
				p.Header.Set("Content-Type", smtpDefaultContentType)
			}
			subct, _, err := mime.ParseMediaType(p.Header.Get("Content-Type"))
			if strings.HasPrefix(subct, "multipart/") {
				sps, err := parseMIMEParts(p.Header, p)
				if err != nil {
					return ps, err
				}
				ps = append(ps, sps...)
			} else {
				if _, err := io.Copy(&buf, p); err != nil {
					return ps, err
				}
				ps = append(ps, &part{body: buf.Bytes(), header: p.Header})
			}
		}
	} else {
		// """Singlepart""" (Ca se dit ca ?)
		var buf bytes.Buffer
		if _, err := io.Copy(&buf, b); err != nil {
			return ps, err
		}
		ps = append(ps, &part{body: buf.Bytes(), header: hs})
	}
	return ps, nil
}

func (e *SMTPEmail) AttachBuffer(b bytes.Buffer, filename string, c string) (a *Attachment, err error) {
	at := &Attachment{
		Filename: filename,
		Header:   textproto.MIMEHeader{},
		Content:  b.Bytes(),
	}
	// Get the Content-Type to be used in the MIMEHeader
	if c != "" {
		at.Header.Set("Content-Type", c)
	} else {
		// If the Content-Type is blank, set the Content-Type to "application/octet-stream"
		at.Header.Set("Content-Type", "application/octet-stream")
	}
	at.Header.Set("Content-Disposition", fmt.Sprintf("attachment;\r\n filename=\"%s\"", filename))
	at.Header.Set("Content-ID", fmt.Sprintf("<%s>", filename))
	at.Header.Set("Content-Transfer-Encoding", "base64")
	e.Attachments = append(e.Attachments, at)
	return at, nil
}

func (e *SMTPEmail) AttachReader(r io.Reader, filename string, c string) (a *Attachment, err error) {
	var buffer bytes.Buffer
	if _, err = io.Copy(&buffer, r); err != nil {
		return
	}
	return e.AttachBuffer(buffer, filename, c)

}

// Leethium only
func (e *SMTPEmail) AttachFile(filename string) (a *Attachment, err error) {
	f, err := os.Open(filename)
	if err != nil {
		return
	}
	ct := mime.TypeByExtension(filepath.Ext(filename))
	basename := filepath.Base(filename)
	return e.AttachReader(f, basename, ct)
}

// Construire headers
func (e *SMTPEmail) msgHeaders() (textproto.MIMEHeader, error) {
	res := make(textproto.MIMEHeader, len(e.Headers)+4)
	if e.Headers != nil {
		for _, h := range []string{"To", "Cc", "From", "Subject", "Date", "Message-Id"} {
			if v, ok := e.Headers[h]; ok {
				res[h] = v
			}
		}
	}

	if _, ok := res["To"]; !ok && len(e.To) > 0 {
		res.Set("To", strings.Join(e.To, ", "))
	}
	if _, ok := res["Cc"]; !ok && len(e.Cc) > 0 {
		res.Set("Cc", strings.Join(e.Cc, ", "))
	}
	if _, ok := res["Subject"]; !ok && e.Subject != "" {
		res.Set("Subject", e.Subject)
	}
	if _, ok := res["Message-Id"]; !ok {
		id, err := generateMessageID()
		if err != nil {
			return nil, err
		}
		res.Set("Message-Id", id)
	}
	// Date and From are required headers.
	if _, ok := res["From"]; !ok {
		res.Set("From", e.From)
	}
	if _, ok := res["Date"]; !ok {
		res.Set("Date", time.Now().Format(time.RFC1123Z))
	}
	if _, ok := res["Mime-Version"]; !ok {
		res.Set("Mime-Version", "1.0")
	}
	for field, vals := range e.Headers {
		if _, ok := res[field]; !ok {
			res[field] = vals
		}
	}
	return res, nil
}

// Bytes Construit les objets email
func (e *SMTPEmail) Bytes() ([]byte, error) {
	// TODO: better guess buffer size
	buff := bytes.NewBuffer(make([]byte, 0, 4096))

	headers, err := e.msgHeaders()
	if err != nil {
		return nil, err
	}
	w := multipart.NewWriter(buff)

	// TODO: AutoDetermination ?
	headers.Set("Content-Type", "multipart/mixed;\r\n boundary="+w.Boundary())
	headerToBytes(buff, headers)
	io.WriteString(buff, "\r\n")

	fmt.Fprintf(buff, "--%s\r\n", w.Boundary())
	header := textproto.MIMEHeader{}
	if len(e.Text) > 0 || len(e.HTML) > 0 {
		subWriter := multipart.NewWriter(buff)
		header.Set("Content-Type", fmt.Sprintf("multipart/alternative;\r\n boundary=%s\r\n", subWriter.Boundary()))
		headerToBytes(buff, header)
		if len(e.Text) > 0 {
			header.Set("Content-Type", fmt.Sprintf("text/plain; charset=UTF-8"))
			header.Set("Content-Transfer-Encoding", "quoted-printable")
			if _, err := subWriter.CreatePart(header); err != nil {
				return nil, err
			}
			qp := quotedprintable.NewWriter(buff)
			if _, err := qp.Write(e.Text); err != nil {
				return nil, err
			}
			if err := qp.Close(); err != nil {
				return nil, err
			}
		}
		if len(e.HTML) > 0 {
			header.Set("Content-Type", fmt.Sprintf("text/html; charset=UTF-8"))
			header.Set("Content-Transfer-Encoding", "quoted-printable")
			if _, err := subWriter.CreatePart(header); err != nil {
				return nil, err
			}
			qp := quotedprintable.NewWriter(buff)
			if _, err := qp.Write(e.HTML); err != nil {
				return nil, err
			}
			if err := qp.Close(); err != nil {
				return nil, err
			}
		}
		if err := subWriter.Close(); err != nil {
			return nil, err
		}
	}

	for _, a := range e.Attachments {
		ap, err := w.CreatePart(a.Header)
		if err != nil {
			return nil, err
		}
		base64Wrap(ap, a.Content)
	}
	if err := w.Close(); err != nil {
		return nil, err
	}
	return buff.Bytes(), nil
}

func (e *SMTPEmail) send(addr string, a smtp.Auth) error {
	to := make([]string, 0, len(e.To)+len(e.Cc)+len(e.Bcc))
	to = append(append(append(to, e.To...), e.Cc...), e.Bcc...)
	for i := 0; i < len(to); i++ {
		addr, err := mail.ParseAddress(to[i])
		if err != nil {
			return err
		}
		to[i] = addr.Address
	}
	if e.From == "" || len(to) == 0 {
		return errors.New("Must specify at least one From address and one To address")
	}
	from, err := mail.ParseAddress(e.From)
	if err != nil {
		return err
	}
	raw, err := e.Bytes()
	if err != nil {
		return err
	}

	c, err := smtp.Dial(addr)
	if err != nil {
		return err
	}
	defer c.Close()
	hsn, _ := os.Hostname()
	err = c.Hello(hsn)
	if err != nil {
		return err
	}
	if ok, _ := c.Extension("STARTTLS"); ok {
		if err = c.StartTLS(&tls.Config{InsecureSkipVerify: true}); err != nil {
			return err
		}
	}
	if a != nil {
		err = c.Auth(a)
		if err != nil {
			return err
		}
	}
	if err = c.Mail(from.Address); err != nil {
		return err
	}
	for _, addr := range to {
		if err = c.Rcpt(addr); err != nil {
			return err
		}
	}
	w, err := c.Data()
	if err != nil {
		return err
	}
	_, err = w.Write(raw)
	if err != nil {
		return err
	}
	err = w.Close()
	if err != nil {
		return err
	}
	return c.Quit()
}

type Attachment struct {
	Filename string
	Header   textproto.MIMEHeader
	Content  []byte
}

// Wrap de 76 CF RFC2045
func base64Wrap(w io.Writer, b []byte) {
	// 57 raw bytes = 76-byte
	const maxRaw = 57

	buffer := make([]byte, smtpMaxLineLen+len("\r\n"))
	copy(buffer[smtpMaxLineLen:], "\r\n")

	for len(b) >= maxRaw {
		base64.StdEncoding.Encode(buffer, b[:maxRaw])
		w.Write(buffer)
		b = b[maxRaw:]
	}

	if len(b) > 0 {
		out := buffer[:base64.StdEncoding.EncodedLen(len(b))]
		base64.StdEncoding.Encode(out, b)
		out = append(out, "\r\n"...)
		w.Write(out)
	}
}

func headerToBytes(buff *bytes.Buffer, header textproto.MIMEHeader) {
	for field, vals := range header {
		for _, subval := range vals {

			io.WriteString(buff, field)
			io.WriteString(buff, ": ")

			switch {
			case field == "Content-Type" || field == "Content-Disposition":
				buff.Write([]byte(subval))
			default:
				buff.Write([]byte(mime.QEncoding.Encode("UTF-8", subval)))
			}
			io.WriteString(buff, "\r\n")
		}
	}
}

var maxBigInt = big.NewInt(math.MaxInt64)

// generateMessageID => RFC 2822
// <1444789264909237300.3464.1819418242800517193@DESKTOP01>
// nanoseconds Epoch
// calling PID
// cryptographically random int64
// The sending hostname
func generateMessageID() (string, error) {
	t := time.Now().UnixNano()
	pid := os.Getpid()
	rint, err := rand.Int(rand.Reader, maxBigInt)
	if err != nil {
		return "", err
	}
	hrnd, err := rand.Int(rand.Reader, big.NewInt(16392))
	if err != nil {
		hrnd = big.NewInt(0)
	}
	h := fmt.Sprintf("ltlib%d", hrnd)
	msgid := fmt.Sprintf("<%d.%d.%d@%s>", t, pid, rint, h)
	return msgid, nil
}
