package providerlib

import "net/http"

type HTTPErr struct {
	Err        string `json:"msg"`
	Code       uint   `json:"code"`
	HTTPCode   int    `json:"-"`
	Additional string `json:"additional"`
}

func (he *HTTPErr) DocumentableName() string {
	return "respBasicJSONError" + he.Err
}

func (hte HTTPErr) D(add string) HTTPErr {
	hte.Additional = add
	return hte
}

func (hte HTTPErr) Quick(resp http.ResponseWriter) {
	pes := &provErrorStruct{
		baseErr: hte,
		resp:    resp,
	}
	pes.Send(hte.Additional)
}
