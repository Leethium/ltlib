package providerlib

import (
	"net/http"

	"gitlab.com/leethium/ltlib/lib/openapi"
	"gitlab.com/leethium/ltlib/lib/routemaker"
)

type provErrorStruct struct {
	resp    http.ResponseWriter
	baseErr HTTPErr
}

func (pp *provErrorStruct) ApplyDocumentation(doc *openapi.OpenApiDoc, op *openapi.Operation) error {
	compName := "respBasicJSONError" + pp.baseErr.Err
	GenerateBasicRespDocRef(
		compName,                        // Component name
		"Basic error : "+pp.baseErr.Err, // Description
		nil,                             // Data Schema
		&HttpGenericJSONResponse{Data: nil, Success: false, Error: &pp.baseErr}, // Example
		doc)

	ApplyDocumentationByRef(compName,
		"application/json",
		openapi.ToResponseCode(uint(pp.baseErr.HTTPCode)),
		doc,
		op)

	return nil
}

func (pp *provErrorStruct) Provide(resp http.ResponseWriter, req *http.Request, prevProvided []any) (output Sender[string], next bool) {
	return &provErrorStruct{
		baseErr: pp.baseErr,
		resp:    resp,
	}, true
}

func (pp *provErrorStruct) Send(additional string) {
	curErr := pp.baseErr
	curErr.Additional = additional
	pp.resp.WriteHeader(curErr.HTTPCode)
	r := &HttpGenericJSONResponse{}
	r.Error = &curErr
	r.SendTo(pp.resp)
}

func ProviderError(httpErr HTTPErr) routemaker.Provider[Sender[string]] {
	out := &provErrorStruct{
		baseErr: httpErr,
	}

	_ = routemaker.Documentable(out) // Ensure struct is documentable
	return out
}
