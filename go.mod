module gitlab.com/leethium/ltlib

go 1.21

require github.com/davecgh/go-spew v1.1.1

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20211218093645-b94a6e3cc137 // indirect
	github.com/go-kit/log v0.2.1 // indirect
	github.com/go-logfmt/logfmt v0.6.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
)

require (
	github.com/aws/aws-sdk-go v1.44.288
	github.com/go-kit/kit v0.12.0
	github.com/go-sql-driver/mysql v1.7.1
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/jmoiron/sqlx v1.3.5
	github.com/julienschmidt/httprouter v1.3.0
	github.com/martinlindhe/base36 v1.1.1
	github.com/rs/xid v1.5.0
	golang.org/x/crypto v0.10.0
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
	gopkg.in/yaml.v3 v3.0.1
)
