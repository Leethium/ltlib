package providerlib

import (
	"encoding/json"
	"net/http"
	"reflect"
	"strings"

	"gitlab.com/leethium/ltlib/lib/ltlib"
	"gitlab.com/leethium/ltlib/lib/openapi"
	"gitlab.com/leethium/ltlib/lib/routemaker"
)

var jsonErrorInput = HTTPErr{"JSONErr", 1002, 400, ""}

type inputJsonProvider[T any] struct {
	data T
}

func (ip *inputJsonProvider[T]) Provide(resp http.ResponseWriter, req *http.Request, prevProvided []any) (output T, next bool) {
	err := json.NewDecoder(req.Body).Decode(ip.data)
	if err != nil {
		jsonErrorInput.D(err.Error()).Quick(resp)
		return ip.data, false
	}
	return ip.data, true
}

func (pp *inputJsonProvider[T]) ApplyDocumentation(doc *openapi.OpenApiDoc, op *openapi.Operation) error {
	compName := "bodyJSONautogen_" + pp.CompName()
	componentName := openapi.ComponentName(compName)
	doc.Components.Schemas[componentName] = ValueToSchema(pp.data)

	if op.RequestBody == nil {
		op.RequestBody = &openapi.RequestBody{}
	}
	if op.RequestBody.Content == nil {
		op.RequestBody.Content = make(map[string]*openapi.MediaType)
	}
	op.RequestBody.Content["application/json"] = &openapi.MediaType{
		Schema: &openapi.Schema{
			REF: string("#/components/schemas/" + compName),
		},
	}

	return nil
}

func JSONInput[T any](data T) routemaker.Provider[T] {
	return &inputJsonProvider[T]{
		data: data,
	}
}

func (pp inputJsonProvider[T]) CompName() string {
	t := reflect.TypeOf(pp.data)
	name := ""
	if t.Kind() == reflect.Ptr {
		name = t.Elem().String()
	}
	name = t.Elem().String()
	if name == "" {
		return "rd" + ltlib.GenerateRandomString(4)
	}
	name = strings.ReplaceAll(name, ".", "_")
	return name
}
